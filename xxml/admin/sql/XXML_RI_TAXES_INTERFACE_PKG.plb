CREATE OR REPLACE
PACKAGE BODY XXML.XXML_RI_TAXES_INTERFACE_PKG AS

  /*
  +===================================================================================================+
  |                                                                                                   |
  | OBJETIVO: RECUPERA O LIMITE PARA RETENCAO DO INSS                                                 |
  |                                                                                                   |
  | PROCEDURE: GET_INSS_ACUMULATED                                                                    |
  |                                                                                                   |
  | CREATE DATE                CREATE BY                                                              |
  | --------------------------------------------------------------------------------------------------+
  | 17-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
  |                            ANALISTA ORACLE/SOA                                                    |
  +===================================================================================================+
  */
  FUNCTION GET_INSS_ACUMULATED(p_entity_id IN CLL_F189_INVOICES.entity_id%TYPE
                              ,p_gl_date   IN CLL_F189_ENTRY_OPERATIONS.gl_date%TYPE) RETURN NUMBER IS
    
    l_inss_auto_tot NUMBER := 0;
  BEGIN
    BEGIN
      SELECT (NVL(cfi.inss_base,0) + NVL(cfi.inss_autonomous_invoiced_total,0))
        INTO l_inss_auto_tot
        FROM cll_f189_invoices         cfi
            ,cll_f189_entry_operations cfeo
       WHERE cfi.organization_id       = cfeo.organization_id
         AND cfi.operation_id          = cfeo.operation_id
         AND cfi.location_id           = cfeo.location_id
         AND cfeo.status               = 'COMPLETE'
         AND cfi.entity_id             = p_entity_id
          AND TRUNC(cfeo.gl_date) BETWEEN TRUNC(SYSDATE) - (TO_NUMBER(TO_CHAR(p_gl_date, 'DD')) - 1)
                                      AND LAST_DAY(p_gl_date);
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
    
    RETURN l_inss_auto_tot;
  END GET_INSS_ACUMULATED;

  /*
  +===================================================================================================+
  |                                                                                                   |
  | OBJETIVO: RECUPERA INFORMACOES DO TIPO DE NF PARA CALCULO DOS IMPOSTOS DA NOTA                    |
  |                                                                                                   |
  | PROCEDURE: CALCULATE_INVOICE_TAXES                                                                |
  |                                                                                                   |
  | CREATE DATE                CREATE BY                                                              |
  | --------------------------------------------------------------------------------------------------+
  | 17-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
  |                            ANALISTA ORACLE/SOA                                                    |
  +===================================================================================================+
  */
  PROCEDURE CALCULATE_INVOICE_TAXES(p_organization_id                 IN CLL_F189_INVOICES.ORGANIZATION_ID%TYPE
                                   ,p_invoice_type_id                 IN CLL_F189_INVOICES.INVOICE_TYPE_ID%TYPE
                                   ,p_invoice_id                      IN CLL_F189_INVOICES.INVOICE_ID%TYPE
                                   ,p_gross_total_amount              IN CLL_F189_INVOICES.GROSS_TOTAL_AMOUNT%TYPE
                                   ,p_entity_id                       IN CLL_F189_INVOICES.ENTITY_ID%TYPE
                                   ,p_gl_date                         IN CLL_F189_ENTRY_OPERATIONS.GL_DATE%TYPE
                                   ,p_invoice_amount              IN OUT CLL_F189_INVOICES.INVOICE_AMOUNT%TYPE
                                   ,p_inss_base                      OUT CLL_F189_INVOICES.INSS_BASE%TYPE
                                   ,p_inss_autonomous_tax            OUT CLL_F189_INVOICES.INSS_AUTONOMOUS_TAX%TYPE
                                   ,p_inss_autonomous_amount         OUT CLL_F189_INVOICES.INSS_AUTONOMOUS_AMOUNT%TYPE
                                   ,p_inss_autonomous_invoiced_tot   OUT CLL_F189_INVOICES.INSS_AUTONOMOUS_INVOICED_TOTAL%TYPE
                                   ,p_inss_withhold_invoice_id       OUT CLL_F189_INVOICES.INSS_WITHHOLD_INVOICE_ID%TYPE
                                   ,p_inss_tax                       OUT CLL_F189_INVOICES.INSS_TAX%TYPE
                                   ,p_sest_senat_base                OUT CLL_F189_INVOICES.SEST_SENAT_BASE%TYPE
                                   ,p_sest_senat_tax                 OUT CLL_F189_INVOICES.SEST_SENAT_TAX%TYPE
                                   ,p_sest_senat_amount              OUT CLL_F189_INVOICES.SEST_SENAT_AMOUNT%TYPE) IS
                                   
    -- Recupera informa��es do tipo de NF para c�lculo dos impostos da nota
    CURSOR c_invoice_type IS
      SELECT remuneration_freight
            ,inss_calculation_flag
            ,inss_autonomous_tax
            ,include_sest_senat_flag
            ,sest_senat_tax
            ,inss_tax
        FROM cll_f189_invoice_types
       WHERE organization_id = p_organization_id
         AND invoice_type_id = p_invoice_type_id;
         
    l_inv_type_rec              c_invoice_type%ROWTYPE;
    
    l_inss_base                 NUMBER := 0;
    l_inss_faturado             NUMBER := 0;
    l_inss_retencao             NUMBER := 0;
    l_inss_autonomous_invoiced  NUMBER := 0;
    l_inss_autono_max_retention NUMBER := 0;
    
    /** Al�quota para c�lculo do INSS Aut�nomo */
    c_remuneration_freight      CONSTANT NUMBER DEFAULT 20;
    c_inss_autonomous_legal_tax CONSTANT NUMBER DEFAULT 11;
    c_sest_legal_tax            CONSTANT NUMBER DEFAULT 1.5;
    c_senat_legal_tax           CONSTANT NUMBER DEFAULT 1.0;
         
  BEGIN
  
    OPEN c_invoice_type;
    LOOP
      FETCH c_invoice_type INTO l_inv_type_rec;
      EXIT WHEN c_invoice_type%NOTFOUND;
      
        -- Apura o valor base de INSS para c�lculo do INSS e SEST/SENAT
        IF (l_inv_type_rec.remuneration_freight IS NOT NULL) THEN
          l_inss_base := ROUND(((p_gross_total_amount * l_inv_type_rec.remuneration_freight) / 100), 2);
        ELSE
          -- Quando o remuneration freight estiver nulo, indica que a base de c�lculo n�o deve ser reduzida.
          -- Essa regra se aplica para MLVOCE
          l_inss_base := p_gross_total_amount;
        END IF;
    
        -- Verifica se � necess�rio o c�lculo do INSS
        IF (NVL(l_inv_type_rec.inss_calculation_flag, 'N') = 'N') THEN
          p_inss_base                    := 0;
          p_inss_autonomous_tax          := 0;
          p_inss_autonomous_amount       := 0;
          p_inss_autonomous_invoiced_tot := 0;
          p_inss_withhold_invoice_id     := NULL;
        ELSE
        
          -- Recupera valor acumulado no INSS
          l_inss_autonomous_invoiced := XXML_RI_TAXES_INTERFACE_PKG.get_inss_acumulated(p_entity_id, p_gl_date);
          
          -- Recupera o limite para reten��o do INSS
          SELECT inss_autonomous_max_retention
            INTO l_inss_autono_max_retention
            FROM cll_f189_parameters
           WHERE organization_id = p_organization_id;
           
          -- Al�quota do INSS definida no cadastro do tipo de NF
          p_inss_tax := l_inv_type_rec.inss_tax;

          -- Al�quota descontado do valor do frete para definir o valor base para o INSS aut�nomo e SEST/SENAT
          p_inss_base := l_inss_base;
          p_inss_autonomous_tax := NVL(l_inv_type_rec.inss_autonomous_tax, c_inss_autonomous_legal_tax);

          -- Define o valor do INSS para a nota fiscal
          l_inss_faturado := ROUND(((p_inss_base * p_inss_autonomous_tax) / 100), 2);

          -- Verifica o valor acumulado de INSS do fornecedor para o per�odo
          l_inss_retencao := ROUND(NVL(l_inss_autonomous_invoiced,0) * (p_inss_autonomous_tax / 100), 2);

          -- Verifica se o INSS j� retido ultrapassou o limite de reten��o do per�odo para o fornecedor
          -- Se j� ultrapassou, n�o reter mais INSS para as pr�ximas notas do fornecedor
          IF (l_inss_retencao > l_inss_autono_max_retention) THEN
             p_inss_autonomous_amount := 0;
          -- Verifica se o INSS acumulado mais o que ser� fatura para a nota que esta sendo importada ultrapassou o limite de reten��o
          -- Se sim, retem apenas a diferen�a entre o limite e o acumulado
          ELSIF (l_inss_retencao + l_inss_faturado) > l_inss_autono_max_retention THEN
             p_inss_autonomous_amount       := ROUND((l_inss_autono_max_retention - l_inss_retencao), 2);
             -- Quando ultrapassa o limite de reten��o, o campo de cumulatividade deve ser preenchido
             p_inss_autonomous_invoiced_tot := NVL(l_inss_autonomous_invoiced, 0);
          ELSE
            p_inss_autonomous_amount := l_inss_faturado;
          END IF;

          -- Atualizar campos da tabela para tratar cumulatividade do INSS e reten��o do mesmo.
          IF p_inss_autonomous_amount > 0 THEN
            p_inss_withhold_invoice_id := p_invoice_id;
          END IF;
        
        END IF;
        
        -- Verificar se o Tipo de NFF deve calcular SEST/SENAT e calcula SEST/SENAT.
        IF NVL(l_inv_type_rec.include_sest_senat_flag,'N') = 'N' THEN
          p_sest_senat_base   := 0;
          p_sest_senat_tax    := 0;
          p_sest_senat_amount := 0;
        ELSE
          p_sest_senat_base   := l_inss_base;
          p_sest_senat_tax    := NVL(l_inv_type_rec.sest_senat_tax, (c_sest_legal_tax + c_senat_legal_tax));
          p_sest_senat_amount := NVL(ROUND(((p_sest_senat_base * p_sest_senat_tax) / 100), 2), 0);
        END IF;

        -- Atualiza o valor l�quido da nota deduzindo o INSS e SEST/SENAT da mesma
        p_invoice_amount := p_invoice_amount - p_inss_autonomous_amount - p_sest_senat_amount;
    
    END LOOP;
    CLOSE c_invoice_type;
  END CALCULATE_INVOICE_TAXES;

  /*
  +===================================================================================================+
  |                                                                                                   |
  | OBJETIVO: RECUPERA TARIFA ICMS POR ESTADO                                                         |
  |                                                                                                   |
  | PROCEDURE: ICMS_TAX                                                                               |
  |                                                                                                   |
  | CREATE DATE                CREATE BY                                                              |
  | --------------------------------------------------------------------------------------------------+
  | 17-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
  |                            ANALISTA ORACLE/SOA                                                    |
  +===================================================================================================+
  */
  FUNCTION ICMS_TAX(p_source_state_id IN NUMBER
                   ,p_dest_state_id   IN NUMBER) RETURN NUMBER IS
    l_icms_tax NUMBER;
  BEGIN
    BEGIN
      SELECT rsr.icms_tax
        INTO l_icms_tax
        FROM cll_f189_state_relations rsr
       WHERE rsr.source_state_id      = p_source_state_id
         AND rsr.destination_state_id = p_dest_state_id;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_icms_tax := 0;
    END;
      
    RETURN l_icms_tax;
  END ICMS_TAX;
  
  /*
  +===================================================================================================+
  |                                                                                                   |
  | OBJETIVO: ROTINA ALTERADA PARA N�O CONSIDERAR A NOTA UMA VEZ QUE A MESMA N�O EXISTE.              |
  |           ESSA ROTINA FOI COPIADA DA PACKAGE CLL_F189_TAX_PKG MANTENDO-SE SUA REGRA NATIVA.       |
  |                                                                                                   |
  | PROCEDURE: FIND_ICMS                                                                              |
  |                                                                                                   |
  | CREATE DATE                CREATE BY                                                              |
  | --------------------------------------------------------------------------------------------------+
  | 17-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
  |                            ANALISTA ORACLE/SOA                                                    |
  +===================================================================================================+
  */
  PROCEDURE FIND_ICMS(p_invoice_type_id          IN            NUMBER
                     ,p_source_state_id          IN            NUMBER
                     ,p_destination_state_id     IN            NUMBER
                     ,p_classification_id        IN            NUMBER
                     ,p_utilization_id           IN            NUMBER
                     ,p_line_amount              IN            NUMBER
                     ,p_ipi_amount               IN            NUMBER
                     ,p_add_ipi_to_icms_flag     IN            VARCHAR2
                     ,p_icms_tax                 IN OUT NOCOPY NUMBER
                     ,p_icms_base                   OUT NOCOPY NUMBER
                     ,p_icms_amount                 OUT NOCOPY NUMBER
                     ,p_icms_tax_code               OUT NOCOPY VARCHAR2
                     ,p_recover_icms_amount         OUT NOCOPY NUMBER
                     ,p_diff_icms_tax               OUT NOCOPY NUMBER
                     ,p_diff_icms_amount            OUT NOCOPY NUMBER
                     ,p_diff_icms_amount_recover    OUT NOCOPY NUMBER
                     ,p_entity_id                IN            NUMBER
                     ,p_ship_via_lookup_code     IN            VARCHAR2
                     ,p_icms_type                IN            VARCHAR2) IS
  
    v_exempt_tax_flag           VARCHAR2 (1)                                           := 'Y';
    v_freight_flag              VARCHAR2(1);
    v_reduction_percent         cll_f189_government_rules.reduction_percent%TYPE       := 0;
    v_increase_percent          cll_f189_government_rules.increase_percent%TYPE        := 0;
    v_differ_tax_flag           cll_f189_government_rules.differ_tax_flag%TYPE         := 'N';
    v_different_tax             cll_f189_government_rules.different_tax%TYPE;
    v_suspended_tax_flag        cll_f189_government_rules.suspended_tax_flag%TYPE;
    v_item_diferimento          NUMBER;
    v_per_red_bas_icms          NUMBER                                                 := 0;
    v_include_icms              cll_f189_invoice_types.include_icms_flag%TYPE;
    v_item_excecao              NUMBER;
    v_icms_tax                  cll_f189_state_relations.icms_tax%TYPE;
    v_icms_base                 cll_f189_invoices.icms_base%TYPE;
    v_icms_amount               cll_f189_invoice_lines.icms_amount%TYPE;
    v_icms_tax_code             cll_f189_invoice_lines.icms_tax_code%TYPE;
    v_recover_icms_amount       cll_f189_invoice_lines.icms_amount_recover%TYPE;
    v_diff_icms_amount          cll_f189_invoice_lines.diff_icms_amount%TYPE;
    v_diff_icms_tax             cll_f189_invoice_lines.diff_icms_tax%TYPE;
    v_recover_icms_flag         cll_f189_item_utilizations.recover_icms_flag%TYPE;
    v_diff_icms_flag            cll_f189_item_utilizations.diff_icms_flag%TYPE;
    v_destination_icms_tax      cll_f189_states.internal_icms_tax%TYPE;
    v_source_icms_tax           cll_f189_states.internal_icms_tax%TYPE;
    v_rel_icms_tax              cll_f189_state_relations.icms_tax%TYPE;
    v_rel_freight_icms_tax      cll_f189_state_rel_ship_vias.freight_icms_type%TYPE;
    v_dest_freight_icms_tax     cll_f189_states.freight_icms_tax%TYPE;
    v_source_national_state     cll_f189_states.national_state%TYPE;
    v_diff_icms_amount_rcv      cll_f189_invoice_lines.diff_icms_amount%TYPE;
    v_icms_differed_type        cll_f189_item_utilizations.icms_differed_type%TYPE;
    x_return_customer_flag      cll_f189_invoice_types.return_customer_flag%TYPE;
    x_icms_contributor_flag     cll_f189_business_vendors.icms_contributor_flag%TYPE;
    x_import_icms_flag          cll_f189_invoice_types.import_icms_flag%TYPE;
    v_different_tax_icms        cll_f189_government_rules.different_tax_icms%TYPE;
    v_requisition_type          cll_f189_invoice_types.requisition_type%TYPE;
    v_return_flag               VARCHAR2(3);

    v_organization_id           cll_f189_invoice_types.organization_id%TYPE;
    v_recover_freight_tax       cll_f189_parameters.recover_freight_tax%TYPE;
    --

  BEGIN
    BEGIN
      SELECT cfit.include_icms_flag
            ,cfit.freight_flag
            ,NVL (cfit.icms_reduction_percent, 0)
            ,cfit.return_customer_flag
            ,cfit.import_icms_flag
            ,cfit.requisition_type
            ,cfit.organization_id
            ,cfit.return_customer_flag
        INTO v_include_icms
            ,v_freight_flag
            ,v_per_red_bas_icms
            ,x_return_customer_flag
            ,x_import_icms_flag
            ,v_requisition_type
            ,v_organization_id
            ,v_return_flag
        FROM cll_f189_invoice_types cfit
       WHERE cfit.invoice_type_id = p_invoice_type_id;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_include_icms         := 'Y';
        v_freight_flag         := 'N';
        v_per_red_bas_icms     := 0;
        x_return_customer_flag := 'N';
        x_import_icms_flag     := 'N';
        v_return_flag          := 'XXX';
    END;

    -- Freight Invoice
    BEGIN
      -- internal freight_icms_tax
      SELECT rssv.freight_icms_tax
        INTO v_dest_freight_icms_tax
        FROM cll_f189_state_ship_vias rssv
       WHERE rssv.state_id = p_destination_state_id
         AND rssv.ship_via_lookup_code = p_ship_via_lookup_code
         AND rssv.freight_icms_type = p_icms_type;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        BEGIN
          -- internal icms_tax
          SELECT rs.internal_icms_tax
            INTO v_dest_freight_icms_tax
            FROM cll_f189_states rs
           WHERE rs.state_id = p_destination_state_id;
        EXCEPTION
          WHEN OTHERS THEN
            v_dest_freight_icms_tax := 0;
        END;
    END;

    BEGIN
      -- freight_icms_tax source-destination
      SELECT rsrsv.freight_icms_tax v_rel_freight_icms_tax
        INTO v_rel_freight_icms_tax
        FROM cll_f189_state_rel_ship_vias rsrsv
       WHERE rsrsv.source_state_id = p_source_state_id
         AND rsrsv.destination_state_id = p_destination_state_id
         AND rsrsv.ship_via_lookup_code = p_ship_via_lookup_code
         AND rsrsv.freight_icms_type    = p_icms_type;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_rel_freight_icms_tax := ICMS_TAX(p_source_state_id, p_destination_state_id);
    END;

    -- Common Invoice
    BEGIN
      -- internal icms_tax
      SELECT rd.internal_icms_tax v_destination_icms_tax
        INTO v_destination_icms_tax
        FROM cll_f189_states rd
       WHERE rd.state_id = p_destination_state_id;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_destination_icms_tax := 0;
    END;
    
    v_rel_icms_tax := ICMS_TAX(p_source_state_id, p_destination_state_id);

    BEGIN
      -- verifying national_state
      -- v_source_icms_tax
      SELECT rs.national_state v_source_national_state
            ,rs.internal_icms_tax
        INTO v_source_national_state
            ,v_source_icms_tax
        FROM cll_f189_states rs
       WHERE rs.state_id = p_source_state_id;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_source_national_state := 'Y';
        v_source_icms_tax := 0;
    END;

    IF (v_include_icms = 'Y') THEN
    
      BEGIN
        SELECT rbv.icms_contributor_flag
          INTO x_icms_contributor_flag
          FROM cll_f189_business_vendors rbv
              ,cll_f189_fiscal_entities_all     rfea
         WHERE rfea.entity_id  = p_entity_id
           AND rbv.business_id = rfea.business_vendor_id;
      EXCEPTION
        WHEN OTHERS THEN
          x_icms_contributor_flag := 'N';
      END;
    
      IF (x_icms_contributor_flag = 'N') THEN
        IF (x_return_customer_flag = 'S') THEN
          v_include_icms := 'Y';
        ELSE
          v_include_icms := 'N';
        END IF;
      END IF;
    END IF;

    IF v_include_icms = 'N' THEN
      v_icms_tax_code        := 2;
      v_icms_tax             := 0;
      v_icms_base            := 0;
      v_icms_amount          := 0;
      v_diff_icms_tax        := 0;
      v_diff_icms_amount     := 0;
      v_diff_icms_amount_rcv := 0;
      v_recover_icms_amount  := 0;
    ELSE

      IF v_return_flag = 'F' THEN
        v_icms_tax := p_icms_tax;
      ELSE

        IF (p_source_state_id = p_destination_state_id) THEN
          IF (p_icms_tax IS NOT NULL) THEN
            v_icms_tax := p_icms_tax;
          ELSE
            IF v_freight_flag = 'N' THEN
              v_icms_tax := v_source_icms_tax;
            ELSE
              v_icms_tax := v_dest_freight_icms_tax;
            END IF;
          END IF;
        ELSE

          IF (p_icms_tax IS NOT NULL) THEN
            v_icms_tax := p_icms_tax;
          ELSE
            IF (v_freight_flag = 'N') THEN
              IF (x_return_customer_flag = 'S') THEN
                IF (x_icms_contributor_flag = 'Y') THEN
                  v_icms_tax := NVL (v_rel_icms_tax, 0);

                  IF (v_icms_tax = 0) THEN
                    IF (v_source_national_state = 'N') THEN
                      v_icms_tax := v_destination_icms_tax;
                    ELSE
                      v_icms_tax := 0;
                    END IF;
                  END IF;
                  --

                ELSE
                  v_icms_tax := NVL (v_destination_icms_tax, 0);

                END IF;
              ELSE
                v_icms_tax := NVL (v_rel_icms_tax, 0);

                IF (v_icms_tax = 0) THEN
                  IF (v_source_national_state = 'N') THEN
                    v_icms_tax := v_destination_icms_tax;
                  ELSE
                    v_icms_tax := 0;
                  END IF;
                END IF;
              END IF;
            ELSE
              v_icms_tax := v_rel_freight_icms_tax;
              --
            END IF;
          END IF;
        END IF;

        BEGIN
          SELECT MAX(rgr.reduction_percent)
                ,MIN(rgr.increase_percent)
                ,MAX(rgr.different_tax)
                ,MAX(different_tax_icms)
            INTO v_reduction_percent
                ,v_increase_percent
                ,v_different_tax
                ,v_different_tax_icms
            FROM cll_f189_fiscal_exceptions rfe
                ,cll_f189_government_rules  rgr
           WHERE rfe.classification_id        = p_classification_id
             AND rfe.rule_id                  = rgr.rule_id
             AND TRUNC(SYSDATE)              >= NVL (rgr.begin_effective_date, TRUNC(SYSDATE))
             AND TRUNC(SYSDATE)              <= NVL(rgr.end_effective_date, TRUNC(SYSDATE))
             AND NVL(rgr.utilization_id, p_utilization_id)
                                              = p_utilization_id
             AND rgr.tax_type                 = 'ICMS'
             AND (rgr.state_id                IS NULL
              OR rgr.state_id                 = p_source_state_id)
             AND (rgr.destination_state_id    IS NULL
              OR rgr.destination_state_id     = p_destination_state_id);
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_exempt_tax_flag := 'N';
        END;

        IF v_reduction_percent IS NULL THEN
          v_reduction_percent := 0;
        END IF;

        IF v_increase_percent IS NULL THEN
          v_increase_percent := 0;
        END IF;

        IF NVL (v_different_tax, 0) > 0 THEN
          v_icms_tax := v_different_tax;
        END IF;

        IF p_source_state_id = p_destination_state_id
           OR v_source_national_state = 'N' THEN
          SELECT COUNT (*)
            INTO v_item_diferimento
            FROM cll_f189_fiscal_exceptions rfe
                ,cll_f189_government_rules rgr
           WHERE rfe.classification_id      = p_classification_id
             AND rfe.rule_id                = rgr.rule_id
             AND TRUNC(SYSDATE)            >= NVL(rgr.begin_effective_date, TRUNC(SYSDATE))
             AND TRUNC(SYSDATE)            <= NVL(rgr.end_effective_date, TRUNC(SYSDATE))
             AND rgr.tax_type               = 'ICMS'
             AND NVL(rgr.utilization_id, p_utilization_id)
                                            = p_utilization_id
             AND (rgr.state_id              IS NULL
              OR rgr.state_id               = p_source_state_id)
             AND (rgr.destination_state_id  IS NULL
              OR rgr.destination_state_id   = p_destination_state_id)
             AND rgr.differ_tax_flag        = 'Y';

          IF v_item_diferimento > 0 THEN
            v_differ_tax_flag := 'Y';
          END IF;

          SELECT MIN(rgr.different_tax)
            INTO v_different_tax
            FROM cll_f189_fiscal_exceptions rfe
                ,cll_f189_government_rules rgr
           WHERE rfe.classification_id          = p_classification_id
             AND rfe.rule_id                    = rgr.rule_id
             AND TRUNC(SYSDATE)                >= NVL(rgr.begin_effective_date, TRUNC(SYSDATE))
             AND TRUNC(SYSDATE)                <= NVL(rgr.end_effective_date, TRUNC(SYSDATE))
             AND rgr.tax_type                   = 'ICMS'
             AND NVL (rgr.utilization_id, p_utilization_id)
                                                = p_utilization_id
             AND (rgr.state_id                  IS NULL
              OR rgr.state_id = p_source_state_id)
             AND (rgr.destination_state_id      IS NULL
              OR rgr.destination_state_id       = p_destination_state_id);
        END IF;
          
        IF v_exempt_tax_flag = 'Y' THEN
          v_icms_tax_code    := '2';
          v_icms_tax         := 0;
          v_per_red_bas_icms := 100;
        ELSE
          IF v_differ_tax_flag = 'Y' OR v_suspended_tax_flag = 'Y' THEN
            v_icms_tax_code    := '3';
            v_icms_tax         := 0;
            v_per_red_bas_icms := 100;
          ELSE
            IF v_different_tax IS NOT NULL THEN
              v_icms_tax := v_different_tax;
            END IF;

            IF v_increase_percent > 0 THEN
              v_per_red_bas_icms := v_increase_percent * -1;
            END IF;

            IF v_reduction_percent > 0 THEN
              v_per_red_bas_icms := v_reduction_percent;
            END IF;
          END IF;
        END IF;
      END IF;
    
      IF p_add_ipi_to_icms_flag = 'Y' THEN
        v_icms_base := (p_line_amount + p_ipi_amount) * (1 - (v_per_red_bas_icms / 100) );
      ELSE
        v_icms_base := p_line_amount * (1 - (v_per_red_bas_icms / 100) );
      END IF;

      BEGIN
        SELECT riu.recover_icms_flag
              ,riu.diff_icms_flag
              ,riu.icms_differed_type
          INTO v_recover_icms_flag
              ,v_diff_icms_flag
              ,v_icms_differed_type
          FROM cll_f189_item_utilizations riu
         WHERE riu.utilization_id = p_utilization_id;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          v_recover_icms_flag := 'N';
          v_diff_icms_flag    := 'N';
      END;

      IF v_icms_differed_type = 'D' THEN
        v_icms_base := v_icms_base / (1 - (v_icms_tax / 100));
      ELSIF x_import_icms_flag = 'D' THEN
        v_icms_base := ROUND(v_icms_base / ((100 - v_icms_tax) / 100), 5);
      END IF;

      v_icms_amount := v_icms_base * (v_icms_tax / 100);
      
      BEGIN
       SELECT NVL(recover_freight_tax,'Y')
         INTO v_recover_freight_tax
         FROM   cll_f189_parameters
         WHERE  organization_id = v_organization_id;
      EXCEPTION
        WHEN OTHERS THEN
          v_recover_freight_tax := 'Y';
      END;

      IF (v_recover_icms_flag = 'Y')
        OR (v_freight_flag = 'Y' and v_recover_freight_tax = 'Y') THEN
        v_recover_icms_amount := v_icms_amount;
      ELSE
        v_recover_icms_amount := 0;
      END IF;

      IF p_source_state_id = p_destination_state_id THEN
        v_diff_icms_flag := 'N';
      ELSE
        IF v_source_national_state = 'N' THEN
          v_diff_icms_flag := 'N';
        END IF;
      END IF;

      IF v_requisition_type = 'RM' THEN
        v_diff_icms_flag := 'N';
      END IF;

      IF v_diff_icms_flag IN ('S', 'I') THEN

        IF v_different_tax_icms IS NOT NULL THEN
          v_diff_icms_tax := v_different_tax_icms;
        ELSE

          IF v_freight_flag = 'N' THEN
            v_diff_icms_tax := v_destination_icms_tax - v_icms_tax;
          ELSE
            v_diff_icms_tax := v_dest_freight_icms_tax - v_icms_tax;
          END IF;
        END IF;

        IF (v_diff_icms_tax < 0) THEN
          v_diff_icms_tax := 0;
        END IF;

        v_diff_icms_amount := v_icms_base * (v_diff_icms_tax / 100);

        IF (v_diff_icms_flag = 'I') THEN
          v_diff_icms_amount_rcv := v_icms_base * (v_diff_icms_tax / 100);
        ELSE
          v_diff_icms_amount_rcv := 0;
        END IF;
      ELSE
        v_diff_icms_amount     := 0;
        v_diff_icms_tax        := 0;
        v_diff_icms_amount_rcv := 0;
      END IF;

      IF v_icms_tax_code IS NULL THEN
        IF v_icms_amount = 0 THEN
          v_icms_tax_code := '3';
        ELSE
          IF v_recover_icms_amount > 0 THEN
            v_icms_tax_code := '1';
          ELSE
            v_icms_tax_code := '3';
          END IF;
        END IF;
      END IF;
    END IF;

    p_icms_base                := NVL(ROUND(v_icms_base, 5), 0);
    p_icms_tax                 := NVL(v_icms_tax, 0);
    p_icms_amount              := NVL(ROUND(v_icms_amount, 5), 0);
    p_recover_icms_amount      := NVL(ROUND(v_recover_icms_amount, 5), 0);
    p_diff_icms_tax            := NVL(v_diff_icms_tax, 0);
    p_diff_icms_amount         := ROUND(v_diff_icms_amount, 5);
    p_icms_tax_code            := NVL(v_icms_tax_code, 0);
    p_diff_icms_amount_recover := NVL(ROUND(v_diff_icms_amount_rcv, 5), 0);
    
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END FIND_ICMS;
  
END XXML_RI_TAXES_INTERFACE_PKG;
