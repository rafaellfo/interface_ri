-- DEPENDENCIAS
    -- Tabelas
        -- XXML_PARAM_HEADERS
        -- XXML_PARAM_LINES
    -- Tipos
        -- XXML_XXML_RI_NF_HEA_REC_TYPE
        -- XXML_XXML_RI_NF_LIN_REC_TYPE
    -- Packages
        -- XXML_OEBS_UTILS_PKG
        -- XXML_RI_TAXES_INTERFACE_PKG
        -- XXML_FIN_INT_PARAMETER_PKG
    -- Usuario Appl / Responsabilidade
        -- ML_CONCUR_RI / ML_RI_SUPER_USUARIO

CREATE OR REPLACE
PACKAGE XXML.XXML_RI_INTERFACE_PKG AUTHID CURRENT_USER IS

-- +============================================================================+
-- |        Copyright (c) 2015 XXML, Franca, Brasil                             |
-- |                       All rights reserved.                                 |
-- +============================================================================+
-- | FILENAME                                                                   |
-- | XXML_RI_INTERFACE_PKG                                                      |
-- |                                                                            |
-- | PURPOSE                                                                    |
-- |   Package de programas para a interface de notas fiscais RI.               |
-- |                                                                            |
-- | [USAGE]                                                                    |
-- |   Package de programas para a interface de notas fiscais RI                |
-- |                                                                            |
-- | [DESCRIPTION]                                                              |
-- |   Package de programas para a interface de notas fiscais RI                |
-- |                                                                            |
-- | CREATED BY                                                                 |
-- |   Diogo Cata Preta           (15/01/2017)                                  |
-- |                                                                            |
-- | ALTERED BY                                                                 |
-- |   [Nome]                     (dd/mm/yyyy)                                  |
-- |      [comentarios sobre a alteracao]                                       |
-- +============================================================================+

    TYPE r_fiscal_entities_rec_type IS RECORD(
                entity_id       NUMBER,
                vendor_site_id  NUMBER,
                source_state_id NUMBER,
                business_id     NUMBER,
                awt_group_id    NUMBER
                );

    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: PROCEDURE PARA CRIACAO DA NOTA NA INTERFACE DO RI.                                      |
    |                                                                                                   |
    | PROCEDURE: CRIAR_NOTA                                                                   |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 24-01-2017                 DIOGO ALCANTARA A. CATA PRETA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    PROCEDURE CRIAR_NOTA(pSource            IN VARCHAR2,
                         pUUID_Protocol     IN VARCHAR2,
                         pNotaRI            IN XXML_RI_NF_HEA_REC_TYPE,
                         pNeedApproval     OUT VARCHAR2,
                         pInterfaceRIId    OUT VARCHAR2,
                         pStatus           OUT VARCHAR2,
                         pHttpResponse     OUT NUMBER,
                         pErrCode          OUT VARCHAR2,
                         pErrBuffer        OUT VARCHAR2);

    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: PROCEDURE PARA APROVAR AS NOTAS INCOMPLETAS DADOS ANTES DE INSERIR NA INTERFACE DO RI.  |
    |                                                                                                   |
    | PROCEDURE: VALIDAR_RI_NOTA                                                                        |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 24-01-2017                 DIOGO ALCANTARA A. CATA PRETA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    PROCEDURE APROVAR_RI_NOTA(pOperationId        IN NUMBER,
                              pOrganizationId     IN NUMBER,
                              p_ErrCode          OUT VARCHAR2,
                              p_cod_retorno      OUT NUMBER,
                              p_msg_retorno      OUT VARCHAR2);

END XXML_RI_INTERFACE_PKG;
/

CREATE SYNONYM XXML_RI_INTERFACE_PKG FOR XXML.XXML_RI_INTERFACE_PKG
/
EXIT
