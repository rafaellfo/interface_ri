CREATE OR REPLACE 
PACKAGE BODY XXML.XXML_RI_INTERFACE_PKG IS

    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: ATUALIZA TABELA PROTOCOL HISTORY                                                        |
    |                                                                                                   |
    | PROCEDURE: UPDATE_PROTOCOL_HISTORY                                                                |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 17-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    PROCEDURE UPDATE_PROTOCOL_HISTORY(pReqHistory IN XXML_OEBS_PROTOCOL_HISTORY%ROWTYPE) IS
      v_result_hist BOOLEAN;
    BEGIN
       IF XXML_OEBS_UTIL_PKG.EXIST_UUID(pReqHistory.uuid_protocol) THEN
         v_result_hist := XXML_OEBS_UTIL_PKG.update_protocol_history(pReqHistory);
       ELSE
         v_result_hist := XXML_OEBS_UTIL_PKG.rec_protocol_history(pReqHistory);
       END IF;
                
    END UPDATE_PROTOCOL_HISTORY;
    
    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: RECUPERA NOME DO FLEXFIELD DESCRITIVO.                                                  |
    |                                                                                                   |
    | PROCEDURE: GET_DESCRIPTIVE_ATTRIBUTE                                                              |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 20-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    FUNCTION GET_DESCRIPTIVE_ATTRIBUTE(p_col IN VARCHAR2) RETURN VARCHAR2 IS
      l_field_name FND_DESCR_FLEX_COLUMN_USAGES.end_user_column_name%TYPE;
    BEGIN
      BEGIN
        SELECT end_user_column_name
          INTO l_field_name
          FROM fnd_descr_flex_column_usages
         WHERE descriptive_flex_context_code = 'Conta de Energia El�trica'
           AND application_column_name = UPPER(p_col);
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
      END;
           
      RETURN l_field_name;
        
    END GET_DESCRIPTIVE_ATTRIBUTE;
    
    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: PROCEDURE PARA CRIACAO DA NOTA NA INTERFACE DO RI.                                      |
    |                                                                                                   |
    | PROCEDURE: GET_EXPENSE_ACCOUNT                                                                    |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 17-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    PROCEDURE GET_EXPENSE_ACCOUNT(p_source             IN VARCHAR2
                                 ,p_organization_id    IN NUMBER
                                 ,p_item_id            IN NUMBER
                                 ,p_cd_filial_contabil IN VARCHAR2 DEFAULT NULL
                                 ,x_ccid              OUT NUMBER
                                 ,p_ErrCode           OUT  VARCHAR2
                                 ,p_cod_retorno       OUT  NUMBER
                                 ,p_msg_retorno       OUT  VARCHAR2) IS

      l_ledger_id           NUMBER;
      l_organization_code   ORG_ORGANIZATION_DEFINITIONS.organization_code%TYPE;
      l_org_id              NUMBER;
      
      l_segment1            GL_CODE_COMBINATIONS.segment1%TYPE;
      l_segment2            GL_CODE_COMBINATIONS.segment2%TYPE;
      l_segment3            GL_CODE_COMBINATIONS.segment3%TYPE;
      l_segment4            GL_CODE_COMBINATIONS.segment4%TYPE;
      l_segment5            GL_CODE_COMBINATIONS.segment5%TYPE;
      l_segment6            GL_CODE_COMBINATIONS.segment6%TYPE;
      l_segment7            GL_CODE_COMBINATIONS.segment7%TYPE;
      l_segment8            GL_CODE_COMBINATIONS.segment8%TYPE;
      l_segment9            GL_CODE_COMBINATIONS.segment9%TYPE;
      
      l_concat_segs         GL_CODE_COMBINATIONS_KFV.concatenated_segments%TYPE;
    BEGIN
      p_cod_retorno := 0;
      
      SELECT organization_code
            ,operating_unit
        INTO l_organization_code
            ,l_org_id
        FROM org_organization_definitions
       WHERE organization_id = p_organization_id;
      
      BEGIN
        SELECT ledger_id
          INTO l_ledger_id
          FROM gl_ledgers
         WHERE name = 'ML_CORPORATIVO';
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          p_cod_retorno  := 1;
          p_msg_retorno  := 'Erro ao recuperar o conjunto de livros contabeis';
          GOTO is_error;
      END;
      
      BEGIN
        SELECT gcc.segment1
              ,gcc.segment6
              ,gcc.segment7
              ,gcc.segment8
              ,gcc.segment9
          INTO l_segment1
              ,l_segment6
              ,l_segment7
              ,l_segment8
              ,l_segment9
          FROM per_all_assignments_f    paaf
              ,gl_code_combinations     gcc
         WHERE paaf.default_code_comb_id = gcc.code_combination_id
           AND paaf.business_group_id    = 0
           AND paaf.person_id            = FND_PROFILE.value('XXML_COMPRADOR_PADRAO_INTEGRACAO')
           AND paaf.primary_flag         = 'Y'
           AND TRUNC(SYSDATE)      BETWEEN paaf.effective_start_date
                                       AND paaf.effective_end_date;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          p_cod_retorno  := 1;
          p_msg_retorno  := 'Erro ao recuperar a combina��o cont�bil padr�o do comprador';
          GOTO is_error;
      END;
      
      l_segment2 := LPAD(NVL(p_cd_filial_contabil, 0), 4, '0');
      
      BEGIN
        SELECT /*+RULE */
               prea.segment_value
          INTO l_segment3
          FROM po_rule_expense_accounts prea
             , fnd_id_flex_segments_vl  fifsv
             , mtl_item_categories_v    miv
         WHERE fifsv.application_id  = (SELECT application_id
                                          FROM fnd_application
                                         WHERE application_short_name = 'SQLGL')
           AND fifsv.id_flex_code    = 'GL#'
           AND prea.segment_num      = fifsv.application_column_name
           AND prea.rule_value_id    = miv.category_id
           AND miv.organization_id   = p_organization_id
           AND miv.inventory_item_id = p_item_id
           AND prea.org_id           = l_org_id;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          p_cod_retorno  := 1;
          p_msg_retorno  := 'Erro ao recuperar a conta de despesa da categoria do item para a organiza��o '||l_organization_code;
          GOTO is_error;
      END;
      
      l_segment4 := LPAD(l_organization_code, 4, '0');
      
      BEGIN
        SELECT CASE
                 WHEN p_source = 'FRETE' THEN
                   attribute1 
                 WHEN p_source = 'MLVOCE' THEN
                   attribute2 
               END segment5
          INTO l_segment5
          FROM cll_f189_parameters
         WHERE organization_id    = p_organization_id
           AND attribute_category = 'INTEGRACAO FINANCEIRA';
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          p_cod_retorno  := 1;
          p_msg_retorno  := 'Erro ao recuperar o centro de resultado para a organiza��o '||l_organization_code;
          GOTO is_error;
      END;
      
      <<is_error>>
      IF (p_cod_retorno > 0) THEN
        p_ErrCode := '4000';
        RETURN;
      END IF;
      
      l_concat_segs := l_segment1||'.'||
                       l_segment2||'.'||
                       l_segment3||'.'||
                       l_segment4||'.'||
                       l_segment5||'.'||
                       l_segment6||'.'||
                       l_segment7||'.'||
                       l_segment8||'.'||
                       l_segment9;
                       
      x_ccid := XXML_OEBS_UTIL_PKG.get_ccid(l_concat_segs, l_ledger_id);

    END GET_EXPENSE_ACCOUNT;
    
    
    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: RECUPERAR INFORMA��ES DA ENTIDADE FISCAL DO FORNECEDOR.                                 |
    |                                                                                                   |
    | PROCEDURE: BUSCA_FORNECEDOR                                                                       |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 24-01-2017                 DIOGO ALCANTARA A. CATA PRETA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    |                                                                                                   |
    | UPDATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 12-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    PROCEDURE BUSCA_ENTIDADE_FISCAL(pSource        IN CHAR,
                                    pCpfCnpj       IN VARCHAR2,
                                    pCllSupplier  OUT r_fiscal_entities_rec_type,
                                    p_ErrCode     OUT VARCHAR2,
                                    p_cod_retorno OUT NUMBER,
                                    p_msg_retorno OUT VARCHAR2) IS
      l_sulfixo_site VARCHAR2(200);
    BEGIN
      p_cod_retorno := 0;
      
      IF pSource = 'FRETE' THEN
        l_sulfixo_site := FND_PROFILE.value('XXML_WILDCARD_LOCAL_FORNEC_FRETE');
      ELSE
        l_sulfixo_site := XXML_FIN_INT_PARAMETER_PKG.get_supplier_suffix('XXML_PGDESPLOJA_FLUXO', pSource);
      END IF;      
      
      BEGIN
        SELECT rffea.entity_id
              ,assa.vendor_site_id
              ,rffea.state_id
              ,cfbv.business_id
              ,aps.awt_group_id
          INTO pCllSupplier.entity_id
              ,pCllSupplier.vendor_site_id
              ,pCllSupplier.source_state_id
              ,pCllSupplier.business_id
              ,pCllSupplier.awt_group_id
          FROM cll_f189_fiscal_entities_all rffea
              ,ap_supplier_sites_all        assa
              ,cll_f189_cities              cfc
              ,cll_f189_states              cfs
              ,cll_f189_business_vendors    cfbv
              ,ap_suppliers                 aps
         WHERE rffea.entity_type_lookup_code = 'VENDOR_SITE'
           AND assa.pay_site_flag            = 'Y'
           AND aps.enabled_flag              = 'Y'
           AND NVL(rffea.org_id, -1)         = NVL (assa.org_id, -1)
           AND assa.vendor_site_id           = rffea.vendor_site_id
           AND aps.vendor_id                 = assa.vendor_id
           AND cfc.city_id (+)               = rffea.city_id
           AND cfc.state_id (+)              = rffea.state_id
           AND cfs.state_id                  = rffea.state_id
           AND cfbv.business_id              = rffea.business_vendor_id
           AND assa.vendor_site_code         = (assa.global_attribute11||assa.global_attribute12||'-'||l_sulfixo_site)
           AND ((assa.global_attribute9      = '1'
           AND  (assa.global_attribute10||assa.global_attribute12)
                                             = LPAD(pCpfCnpj, 11, 0)) -- CPF
            OR  (assa.global_attribute9      = '2'
           AND  (assa.global_attribute10||assa.global_attribute11||assa.global_attribute12)
                                             = LPAD(pCpfCnpj, 15, 0) ) -- CNPJ
               )
           AND (rffea.inactive_date    IS NULL
            OR  rffea.inactive_date         >= TRUNC(SYSDATE));
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          p_cod_retorno := 1;
          p_ErrCode     := '081';
          p_msg_retorno := 'Informa��es fiscais do fornecedor n�o encontradas!';
        WHEN DUP_VAL_ON_INDEX THEN
          p_cod_retorno := 1;
          p_ErrCode     := '082';
          p_msg_retorno := 'Informa��es fiscais do fornecedor duplicadas!';
      END;
    EXCEPTION
      WHEN OTHERS THEN
        p_cod_retorno := 1;
        p_ErrCode     := '083';
        p_msg_retorno := 'Falha consulta Info. Fiscais Fornecedor: '||SQLERRM;
    END BUSCA_ENTIDADE_FISCAL;
    
    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: PROCEDURE PARA VALIDAR DADOS ANTES DE INSERIR NA INTERFACE DO RI.                       |
    |                                                                                                   |
    | PROCEDURE: VALIDAR_RI_NOTA                                                                        |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 24-01-2017                 DIOGO ALCANTARA A. CATA PRETA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    PROCEDURE VALIDAR_RI_NOTA(pNotaRI         IN OUT XXML_RI_NF_HEA_REC_TYPE,
                              pSource             IN VARCHAR2,
                              p_ErrCode          OUT VARCHAR2,
                              p_cod_retorno      OUT NUMBER,
                              p_msg_retorno      OUT VARCHAR2) IS
                              
      l_organization_id         NUMBER;
      l_exist                   VARCHAR2(250);
      l_total_by_line           NUMBER := 0;
      l_count_lines             NUMBER := 0;
      
      e_fatal_error             EXCEPTION;
    BEGIN
      p_cod_retorno := 0;

      --------------------------------------------------------------------------
      -- VALIDACAO PERIODO GL - ABERTO / FECHADO
      --------------------------------------------------------------------------
      l_organization_id := XXML_OEBS_UTILS_PKG.inv_org_from_code_to_id(pNotaRI.branch_office_num);
      
      IF XXML_FIN_INT_PARAMETER_PKG.is_gl_period_closed(SYSDATE, l_organization_id) THEN
        p_ErrCode      := '500|';
        p_msg_retorno  := 'Data contabil fora do Periodo do Contas a Pagar';
      END IF;

      --------------------------------------------------------------------------
      -- VALIDACAO CABECALHO *TODOS
      --------------------------------------------------------------------------
      IF (pNotaRI.branch_office_num IS NULL) THEN
         p_ErrCode      := p_ErrCode||'505|';
         p_msg_retorno  := p_msg_retorno||CHR(10)||'Filial nao informada';
      END IF;
      
      IF (pNotaRI.cpf_cnpj IS NULL) THEN
         p_ErrCode      := p_ErrCode||'510|';
         p_msg_retorno  := p_msg_retorno||CHR(10)||'CPF/CNPJ nao informado';
      END IF;
      
      IF (pNotaRI.gross_total_amount IS NULL) THEN
         p_ErrCode      := p_ErrCode||'515|';
         p_msg_retorno  := p_msg_retorno||CHR(10)||'Valor bruto da nota nao foi informado';
      END IF;
      
      -------------------------------------------------
      -- VALIDACAO ESPECIFICA CONTA DE ENERGIA ELETRICA
      -------------------------------------------------
      IF (pSource = 'CONTA_ENERGIA') THEN
      
        IF (pNotaRI.invoice_date IS NULL) THEN
           p_ErrCode      := p_ErrCode||'520|';
           p_msg_retorno  := p_msg_retorno||CHR(10)||'Data de emissao nao informado';
        END IF;
      
        BEGIN
          SELECT lookup_code
            INTO l_exist
            FROM fnd_lookup_values
           WHERE lookup_type = 'CLL_F189_INVOICE_SERIES'
             AND language    = USERENV('LANG')
             AND lookup_code = pNotaRI.series;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            p_ErrCode      := p_ErrCode||'525|';
            p_msg_retorno  := p_msg_retorno||CHR(10)||'Serie informada invalida';
        END;
      
        IF pNotaRI.cin_attribute2 IS NULL THEN
          p_ErrCode     := p_ErrCode||'530|';
          p_msg_retorno := p_msg_retorno||CHR(10)||'Nao informado '||GET_DESCRIPTIVE_ATTRIBUTE('ATTRIBUTE2');
        END IF;
        
        IF pNotaRI.cin_attribute3 IS NULL THEN
          p_ErrCode     := p_ErrCode||'535|';
          p_msg_retorno := p_msg_retorno||CHR(10)||'Nao informado '||GET_DESCRIPTIVE_ATTRIBUTE('ATTRIBUTE3');
        END IF;
          
        IF pNotaRI.cin_attribute4 IS NULL THEN
          p_ErrCode     := p_ErrCode||'540|';
          p_msg_retorno := p_msg_retorno||CHR(10)||'Nao informado '||GET_DESCRIPTIVE_ATTRIBUTE('ATTRIBUTE4');
        END IF;
        
        IF pNotaRI.cin_attribute5 IS NULL THEN
          p_ErrCode     := p_ErrCode||'545|';
          p_msg_retorno := p_msg_retorno||CHR(10)||'Nao informado '||GET_DESCRIPTIVE_ATTRIBUTE('ATTRIBUTE5');
        END IF;
        
        IF pNotaRI.cin_attribute6 IS NULL THEN
          p_ErrCode     := p_ErrCode||'550|';
          p_msg_retorno := p_msg_retorno||CHR(10)||'Nao informado '||GET_DESCRIPTIVE_ATTRIBUTE('ATTRIBUTE6');
        END IF;
        
        IF pNotaRI.cin_attribute7 IS NULL THEN
          p_ErrCode     := p_ErrCode||'555|';
          p_msg_retorno := p_msg_retorno||CHR(10)||'Nao informado '||GET_DESCRIPTIVE_ATTRIBUTE('ATTRIBUTE7');
        END IF;
        
        IF pNotaRI.cin_attribute8 IS NULL THEN
          p_ErrCode     := p_ErrCode||'560|';
          p_msg_retorno := p_msg_retorno||CHR(10)||'Nao informado '||GET_DESCRIPTIVE_ATTRIBUTE('ATTRIBUTE8');
        END IF;
        
        IF pNotaRI.cin_attribute9 IS NULL THEN
          p_ErrCode     := p_ErrCode||'565|';
          p_msg_retorno := p_msg_retorno||CHR(10)||'Nao informado '||GET_DESCRIPTIVE_ATTRIBUTE('ATTRIBUTE9');
        END IF;
        
        IF pNotaRI.cin_attribute10 IS NULL THEN
          p_ErrCode     := p_ErrCode||'570|';
          p_msg_retorno := p_msg_retorno||CHR(10)||'Nao informado '||GET_DESCRIPTIVE_ATTRIBUTE('ATTRIBUTE10');
        END IF;
        
        IF pNotaRI.cin_attribute11 IS NULL THEN
          p_ErrCode     := p_ErrCode||'575|';
          p_msg_retorno := p_msg_retorno||CHR(10)||'Nao informado '||GET_DESCRIPTIVE_ATTRIBUTE('ATTRIBUTE11');
        END IF;
      
      END IF;
      
      --------------------------------------------------------------------------
      -- VALIDACAO DE LINHAS
      --------------------------------------------------------------------------
      IF  pNotaRI.lines IS NOT NULL AND pNotaRI.lines.COUNT > 0 THEN
        FOR i IN pNotaRI.lines.first .. pNotaRI.lines.last LOOP
          l_count_lines := l_count_lines + 1;
        
          -------------------------------------------------
          -- VALIDACAO ESPECIFICA CONTA DE ENERGIA ELETRICA
          -------------------------------------------------
          IF (pSource = 'CONTA_ENERGIA') THEN
          
            IF (pNotaRI.lines(i).pis_amount IS NULL) THEN
              p_ErrCode      := p_ErrCode||'580|';
              p_msg_retorno  := p_msg_retorno||CHR(10)||'Valor PIS nao informado.';
            END IF;
              
            IF (pNotaRI.lines(i).cofins_amount IS NULL) THEN
              p_ErrCode      := p_ErrCode||'585|';
              p_msg_retorno  := p_msg_retorno||CHR(10)||'Valor COFINS nao informado.';
            END IF;
              
            IF (pNotaRI.lines(i).icms_amount IS NULL) THEN
              p_ErrCode      := p_ErrCode||'590|';
              p_msg_retorno  := p_msg_retorno||CHR(10)||'Valor ICMS nao informado.';
            END IF;

          -------------------------------------------------
          -- VALIDACAO ESPECIFICA FREELANCER
          -------------------------------------------------
          ELSIF (pSource = 'FREELANCER') THEN
          
            IF (pNotaRI.lines(i).purchase_order_num IS NULL) THEN
              p_ErrCode      := p_ErrCode||'581|';
              p_msg_retorno  := p_msg_retorno||CHR(10)||'Ordem de Compra nao informada.';
            END IF;
          
            IF (pNotaRI.lines(i).item IS NULL) THEN
              p_ErrCode      := p_ErrCode||'586|';
              p_msg_retorno  := p_msg_retorno||CHR(10)||'Item nao informado.';
            END IF;
          END IF;
        
          l_total_by_line := l_total_by_line + (NVL(pNotaRI.lines(i).unit_price, 0) * NVL(pNotaRI.lines(i).quantity, 0));
            
        END LOOP;
      END IF;
      
      --------------------------------------------------------------------------
      -- VALIDACOES FINAIS / TOTALIZADORES
      --------------------------------------------------------------------------
      -- Verifica inconsistencia de valor total da nota e linhas, quando mais de uma linha
      -- No caso de apenas uma linha o processo de enriquecimento inclui o valor no preco unitario
      IF (l_count_lines > 1) THEN
        IF (l_total_by_line <> pNotaRI.gross_total_amount) THEN
          p_ErrCode      := p_ErrCode||'595|';
          p_msg_retorno  := p_msg_retorno||CHR(10)||'Divergencia entre valor bruto e valor da(s) linha(s)';
        END IF;
      ELSIF (l_count_lines = 0) THEN
          p_ErrCode      := p_ErrCode||'600|';
          p_msg_retorno  := p_msg_retorno||CHR(10)||'Nenhuma linha informada';
      END IF;
      
      IF (LENGTH(p_ErrCode) > 0) THEN
        RAISE e_fatal_error;
      END IF;

    EXCEPTION
      WHEN e_fatal_error THEN
        p_cod_retorno := 1;
      WHEN OTHERS THEN
        p_ErrCode     := '800';
        p_cod_retorno := 1;
        p_msg_retorno := 'Falha em validacoes: '||SQLERRM;
    END VALIDAR_RI_NOTA;


    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: PROCEDURE PARA VALIDAR DADOS ANTES DE INSERIR NA INTERFACE DO RI.                       |
    |                                                                                                   |
    | PROCEDURE: VALIDAR_RI_NOTA                                                                        |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 24-01-2017                 DIOGO ALCANTARA A. CATA PRETA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    PROCEDURE ENRIQUECER_RI_NOTA(pNotaRI      IN OUT XXML_RI_NF_HEA_REC_TYPE,
                                 pSource          IN VARCHAR2,
                                 pUUID_Protocol   IN VARCHAR2,
                                 p_need_approval OUT VARCHAR2,
                                 p_ErrCode       OUT VARCHAR2,
                                 p_cod_retorno   OUT NUMBER,
                                 p_msg_retorno   OUT VARCHAR2) IS
                                 
        l_cll_supp              R_FISCAL_ENTITIES_REC_TYPE;
        l_custom_param          xxml_param_headers%ROWTYPE;
        
        l_default_date          DATE := TRUNC(SYSDATE);
        l_default_terms_name    AP_TERMS.name%TYPE := 'A VISTA';
        l_id_integracao         NUMBER;
        --l_count_invoice         NUMBER := 0;
        l_po_hdr_id             NUMBER;
        
        l_total_invoice         NUMBER := 0;
        l_add_ipi_to_icms_flag  VARCHAR2(1);
        l_pis_unit_amount       NUMBER;
        l_cofins_unit_amount    NUMBER;
        l_base_amount_pis       NUMBER;
        l_funrural_amount       NUMBER := 0;
        l_other_expenses        NUMBER := 0;
        
        l_cfo_start_code        VARCHAR2(1);
        --l_id_tipo_integracao    VARCHAR2(1);
        l_id_tipo_fornecedor    VARCHAR2(1);
        l_id_tipo_servico       NUMBER;
        l_cll_f189_setup_iss    VARCHAR2(25);
        l_cd_pagto_gps          XXML_PARAM_GERAIS_INTEGRACAO.cd_pagamento_gps%TYPE;
        l_cat_concat_segs       CLL_F189_FISCAL_CLASS.classification_code%TYPE;
        l_operation_fiscal_type CLL_F189_INVOICE_TYPES.operation_fiscal_type%TYPE;
        l_inv_type_code         CLL_F189_INVOICE_TYPES.invoice_type_code%TYPE;

        e_fatal_error           EXCEPTION;

    BEGIN
        XXML_OEBS_UTILS_PKG.initialize('ML_CONCUR_RI', 'ML_RI_SUPER_USUARIO', 'CLL');
        XXML_OEBS_UTILS_PKG.set_policy_context;
        
        ------------------------------------------------------------------------
        -- ATRIBUICOES INICIAIS
        ------------------------------------------------------------------------
        l_id_integracao := ml_int_fechamento_financeiro_s.NEXTVAL;

        IF (pNotaRI.invoice_num IS NULL) THEN
          pNotaRI.invoice_num := l_id_integracao;
        END IF;
        
        IF (pNotaRI.invoice_date IS NULL) THEN
          pNotaRI.invoice_date := l_default_date;
        END IF;
        
        pNotaRI.source                 := pSource||' - '||l_id_integracao;
        pNotaRI.gl_date                := l_default_date;
        pNotaRI.ceo_attribute_category := 'INTEGRACAO';
        pNotaRI.ceo_attribute2         := pUUID_Protocol;
        pNotaRI.ceo_attribute15        := pNotaRI.invoice_num;
    
        ------------------------------------------------------------------------
        -- BUSCA DE DADOS DE ORGANIZACAO
        ------------------------------------------------------------------------
        BEGIN
          SELECT org.organization_id,
                 hrou.location_id
            INTO pNotaRI.organization_id
                ,pNotaRI.location_id
            FROM org_organization_definitions org,
                 hr_organization_units        hrou
           WHERE org.organization_id   = hrou.organization_id
             AND org.organization_code = pNotaRI.branch_office_num
             AND NVL(org.disable_date, TRUNC(SYSDATE))
                                      <= TRUNC(SYSDATE);
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
              p_cod_retorno  := '1005';
              p_msg_retorno  := 'Nao identificado informacoes para a filial '||pNotaRI.branch_office_num;
              RAISE e_fatal_error;
        END;
        
        ------------------------------------------------------------------------
        -- DERIVACOES
        ------------------------------------------------------------------------        
        l_id_tipo_fornecedor := XXML_FIN_INT_PARAMETER_PKG.get_supplier_type(pNotaRI.cpf_cnpj);
        
        l_custom_param       := XXML_FIN_INT_PARAMETER_PKG.get_header_params(p_source    => pSource
                                                                            ,p_supp_type => l_id_tipo_fornecedor);
                                                                 
        pNotaRI.invoice_type_code      := l_custom_param.invoice_type_code;
        pNotaRI.fiscal_document_model  := l_custom_param.fiscal_document_model;
        pNotaRI.icms_type              := l_custom_param.icms_type;
        pNotaRI.cin_attribute_category := l_custom_param.attribute_category;
        l_cd_pagto_gps                 := l_custom_param.income_code;
        pNotaRI.terms_name             := l_custom_param.terms_name;
        
        IF (pNotaRI.series IS NULL) THEN
          pNotaRI.series := l_custom_param.series;
        END IF;
        
        BEGIN
          SELECT inss_tax
                ,ir_vendor
                ,ir_categ
                ,operation_fiscal_type
                ,NVL(attribute1, 'N') need_approval
            INTO pNotaRI.inss_tax
                ,pNotaRI.ir_vendor
                ,pNotaRI.ir_categ
                ,l_operation_fiscal_type
                ,p_need_approval
            FROM cll_f189_invoice_types
           WHERE organization_id   = pNotaRI.organization_id
             AND invoice_type_code = pNotaRI.invoice_type_code;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            p_ErrCode      := '1015';
            p_msg_retorno  := 'Nao identificado informacoes financeiras com derivacoes';
            RAISE e_fatal_error;
        END;
        
        ----------------------------------
        -- ENTIDADE FISCAL / CONCESSONARIA
        ----------------------------------
        BUSCA_ENTIDADE_FISCAL(pSource        => pSource
                             ,pCpfCnpj       => pNotaRI.cpf_cnpj
                             ,pCllSupplier   => l_cll_supp
                             ,p_ErrCode      => p_ErrCode
                             ,p_cod_retorno  => p_cod_retorno
                             ,p_msg_retorno  => p_msg_retorno);
                             
        IF (p_cod_retorno > 0) THEN
          RAISE e_fatal_error;
        END IF;
        
        pNotaRI.entity_id       := l_cll_supp.entity_id;
        pNotaRI.source_state_id := l_cll_supp.source_state_id;

        l_cll_f189_setup_iss := FND_PROFILE.value('CLL_F189_SETUP_ISS');
        
        BEGIN
          SELECT (
            SELECT clfe.city_id
              FROM cll_f189_fiscal_entities_all clfe
                 , cll_f189_cities              clc
             WHERE clfe.city_id                 = clc.city_id
               AND clfe.entity_type_lookup_code = 'VENDOR_SITE'
               AND clfe.entity_id               = l_cll_supp.entity_id
               AND l_cll_f189_setup_iss         = 'SUPPLIER'
            UNION
            SELECT clfe.city_id
              FROM cll_f189_fiscal_entities_all clfe
                 , cll_f189_cities              clc
             WHERE clfe.city_id                 = clc.city_id
               AND clfe.entity_type_lookup_code = 'LOCATION'
               AND clfe.location_id             = pNotaRI.location_id
               AND l_cll_f189_setup_iss         = 'LOCATION'
                 )
          INTO pNotaRI.iss_city_id
          FROM DUAL;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            p_ErrCode      := '1020';
            p_msg_retorno  := 'Nenhum valor encontrado para a Base da Cidade para ISS. Verifique o perfil "CLL F189: Base da cidade para ISS"';
            RAISE e_fatal_error; 
        END;            
        
        ------------------------------------------------------------------------
        -- LOCALIZACAO
        ------------------------------------------------------------------------
        BEGIN
          SELECT cls.state_id
            INTO pNotaRI.destination_state_id
            FROM hr_locations    hrl
               , cll_f189_states cls
           WHERE cls.state_code                = hrl.region_2
             AND hrl.inventory_organization_id = pNotaRI.organization_id;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            pNotaRI.destination_state_id := NULL;
        END;
        
        ------------------------------------------------------------------------
        -- INFORMACOES ESPECIFICAS POR ORIGEM (SOURCE)
        ------------------------------------------------------------------------
        IF (pSource = 'CONTA_ENERGIA') THEN
          IF (l_custom_param.terms_name IS NULL) THEN
            pNotaRI.terms_date := TO_DATE(pNotaRI.cin_attribute5, 'DD/MM/YYYY');
            pNotaRI.terms_name := l_default_terms_name;
          END IF;
        END IF;
        
        ------------------------------------------------------------------------
        -- INFORMACOES FINANCEIRAS PARA INTERFACE
        ------------------------------------------------------------------------        
        IF (pNotaRI.source_state_id = pNotaRI.destination_state_id) THEN
          l_cfo_start_code := '1';
        ELSE
          l_cfo_start_code := '2';
        END IF;

        IF NVL(FND_PROFILE.value('CLL_F189_SETUP_GPS'), 'N') = 'Y' THEN
          pNotaRI.income_code := l_cd_pagto_gps;
        END IF;
        
        -- VALORES
        -- ------------------------------
        -- CALCULA O INSS
        -- ------------------------------
        pNotaRI.inss_base                      := 0;
        pNotaRI.inss_autonomous_tax            := 0;
        pNotaRI.inss_amount                    := 0;
        pNotaRI.inss_autonomous_amount         := 0;
        pNotaRI.inss_autonomous_invoiced_total := 0;

        -- ------------------------------
        -- CALCULA O SEST/SENAT
        -- ------------------------------
        pNotaRI.sest_senat_base   := 0;
        pNotaRI.sest_senat_tax    := 0;
        pNotaRI.sest_senat_amount := 0;
        
        -- Verifica se inclui o ISS
        pNotaRI.iss_base   := 0;
        pNotaRI.iss_tax    := 0;
        pNotaRI.iss_amount := 0;

        -- ------------------------------
        -- CALCULA O IR
        -- ------------------------------
        pNotaRI.ir_base   := 0;
        pNotaRI.ir_tax    := 0;
        pNotaRI.ir_amount := 0;

        -- ------------------------------
        -- CALCULA O ICMS
        -- ------------------------------
        pNotaRI.icms_base   := 0;
        pNotaRI.icms_tax    := 0;
        pNotaRI.icms_amount := 0;

        -- ------------------------------
        -- CALCULA O ICMS ST
        -- ------------------------------
        pNotaRI.icms_st_base             := 0;
        pNotaRI.icms_st_amount           := 0;
        pNotaRI.icms_st_amount_recover   := 0;
        pNotaRI.diff_icms_amount_recover := 0;

        -- ------------------------------
        -- CALCULA O IPI
        -- ------------------------------
        pNotaRI.ipi_amount := 0;

        -- ------------------------------
        -- CALCULA OUTROS VALORES
        -- ------------------------------
        pNotaRI.insurance_amount  := 0;
        pNotaRI.freight_amount    := 0;
        pNotaRI.payment_discount  := 0;
        pNotaRI.additional_amount := 0;
        pNotaRI.additional_tax    := 0;
        pNotaRI.invoice_weight    := 0;
        
        -- ------------------------------
        -- CALCULA VALOR L�QUIDO
        -- Valor L�quido da NF = Valor Bruto
        --                     + Outras Despesas
        --                     - IR
        --                     - INSS
        --                     - SEST/SENAT
        --                     - ISS
        -- ------------------------------
        pNotaRI.invoice_amount := ROUND(NVL(pNotaRI.gross_total_amount, 0)
                                      + NVL(l_other_expenses, 0)
                                      - NVL(pNotaRI.inss_autonomous_amount, 0)
                                      - NVL(pNotaRI.ir_amount,0)
                                      - NVL(pNotaRI.sest_senat_amount, 0)
                                      - NVL(pNotaRI.iss_amount, 0)
                                      - NVL(l_funrural_amount, 0)
                                      , 2 );

        -- ---------------------------------------------------------------------
        -- ENRIQUECIMENTO DAS LINHAS
        -- ---------------------------------------------------------------------
        IF  pNotaRI.lines IS NOT NULL AND pNotaRI.lines.COUNT > 0 THEN
          FOR i IN pNotaRI.lines.first .. pNotaRI.lines.last LOOP
          
            -------------------------------------------
            -- INCREMENTO / ADICAO VALORES
            -------------------------------------------
            -- Insere na linha o valor total do cabecalho - uma vez que ja passou pela validacao de numero de linhas
            IF (pNotaRI.lines(i).unit_price IS NULL) THEN
              pNotaRI.lines(i).unit_price := pNotaRI.gross_total_amount;
            END IF;
            
            l_total_invoice               := NVL(l_total_invoice, 0) + pNotaRI.lines(i).unit_price;
            pNotaRI.lines(i).net_amount   := NVL(pNotaRI.lines(i).quantity, 1) * pNotaRI.lines(i).unit_price;
            pNotaRI.lines(i).total_amount := pNotaRI.lines(i).net_amount;
            
            ------------------------------------------------------------------------
            -- ORDEM DE COMPRA GERADA COM PRECEDENCIA (PO)
            ------------------------------------------------------------------------  
            IF (pNotaRI.lines(i).purchase_order_num IS NOT NULL) THEN  
              pNotaRI.terms_name := NULL;
                      
              BEGIN
                SELECT pha.terms_id,
                       pla.line_num,
                       plla.shipment_num
                  INTO pNotaRI.terms_id
                      ,pNotaRI.lines(i).line_num
                      ,pNotaRI.lines(i).shipment_num
                  FROM po_headers_all           pha,
                       po_lines_all             pla,
                       po_line_locations_all    plla
                 WHERE 1 = 1
                   AND pha.po_header_id   = pla.po_header_id
                   AND plla.po_header_id  = pha.po_header_id
                   AND plla.po_line_id    = pla.po_line_id
                   AND pha.segment1       = pNotaRI.lines(i).purchase_order_num
                   AND pha.vendor_site_id = l_cll_supp.vendor_site_id;
              EXCEPTION
                WHEN NO_DATA_FOUND THEN
                  p_ErrCode      := '1030';
                  p_msg_retorno  := 'Nao identificado dados a partir da Ordem de Compra';
                  RAISE e_fatal_error;
              END;
            END IF;
            
            ------------------------------------------------------------------------
            -- DERIVACAO DE ITENS
            ------------------------------------------------------------------------  
            IF (pNotaRI.lines(i).item IS NULL) THEN
              pNotaRI.lines(i).item := XXML_FIN_INT_PARAMETER_PKG.get_lines_param
                                                                      (p_source    => pSource
                                                                      ,p_supp_type => l_id_tipo_fornecedor);                                              
            END IF;

            BEGIN
              SELECT inventory_item_id
                INTO pNotaRI.lines(i).item_id
                FROM mtl_system_items_b
               WHERE organization_id = pNotaRI.organization_id
                 AND segment1        = pNotaRI.lines(i).item;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                p_ErrCode      := '1035';
                p_msg_retorno  := 'Nao identificado item';
                RAISE e_fatal_error;
            END;
               
            --------------------------------------------------------------------
            -- INFORMACOES FINANCEIRAS PARA LINHAS INTERFACE
            --------------------------------------------------------------------
            pNotaRI.lines(i).operation_fiscal_type := l_operation_fiscal_type;

            BEGIN
              SELECT cfiu.utilization_id
                INTO pNotaRI.lines(i).utilization_id
                FROM mtl_system_items_vl        msiv
                    ,cll_f189_item_utilizations cfiu
               WHERE msiv.global_attribute2 = cfiu.utilization_code
                 AND msiv.organization_id   = pNotaRI.organization_id
                 AND msiv.segment1          = pNotaRI.lines(i).item;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                p_ErrCode      := '1040';
                p_msg_retorno  := 'Dados de utilizacao nao identificados';
                RAISE e_fatal_error;
            END;

            BEGIN
              SELECT cfo.cfo_id
                INTO pNotaRI.lines(i).cfo_id
                FROM cll_f189_cfo_utilizations  utl
                    ,cll_f189_fiscal_operations cfo
               WHERE utl.utilization_id = pNotaRI.lines(i).utilization_id
                 AND utl.cfo_id         = cfo.cfo_id
                 AND cfo.cfo_code    LIKE l_cfo_start_code||'%';
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                p_ErrCode      := '1045';
                p_msg_retorno  := 'Dados de CFO nao identificados';
                RAISE e_fatal_error;
            END;

            SELECT muom.unit_of_measure
                  ,msiv.description
                  ,mic.category_concat_segs
              INTO pNotaRI.lines(i).uom
                  ,pNotaRI.lines(i).description
                  ,l_cat_concat_segs
              FROM mtl_system_items_vl      msiv
                  ,mtl_item_categories_v    mic
                  ,mtl_units_of_measure     muom
             WHERE msiv.organization_id    = mic.organization_id
               AND msiv.inventory_item_id  = mic.inventory_item_id
               AND msiv.primary_uom_code   = muom.uom_code
               AND mic.category_set_name   = 'FISCAL_CLASSIFICATION'
               AND msiv.organization_id    = pNotaRI.organization_id
               AND msiv.segment1           = pNotaRI.lines(i).item;    

            BEGIN
              SELECT classification_id
                    ,classification_code
                INTO pNotaRI.lines(i).classification_id
                    ,pNotaRI.lines(i).classification_code
                FROM cll_f189_fiscal_class
               WHERE classification_code = l_cat_concat_segs;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                p_ErrCode      := '1050';
                p_msg_retorno  := 'Nao identificado informacoes de classificacao fiscal';
                RAISE e_fatal_error;
            END;

             -------------------------------------------------------------------
             -- RECUPERA IMPOSTOS / ICMS PIS COFINS PIS
             -------------------------------------------------------------------
             -- Find_ICMS / GET_ICMS
             XXML_RI_TAXES_INTERFACE_PKG.find_icms(p_invoice_type_id             => pNotaRI.invoice_type_id --l_ml_params_rec.id_tipo_nf
                                                 , p_source_state_id             => l_cll_supp.source_state_id
                                                 , p_destination_state_id        => pNotaRI.destination_state_id
                                                 , p_classification_id           => pNotaRI.lines(i).classification_id
                                                 , p_utilization_id              => pNotaRI.lines(i).utilization_id
                                                 , p_line_amount                 => pNotaRI.gross_total_amount -- recebe total : l_total_invoice
                                                 , p_ipi_amount                  => pNotaRI.lines(i).ipi_amount
                                                 , p_add_ipi_to_icms_flag        => l_add_ipi_to_icms_flag
                                                 , p_icms_tax                    => pNotaRI.lines(i).icms_tax
                                                 , p_icms_base                   => pNotaRI.lines(i).icms_base
                                                 , p_icms_amount                 => pNotaRI.lines(i).icms_amount
                                                 , p_icms_tax_code               => pNotaRI.lines(i).icms_tax_code
                                                 , p_recover_icms_amount         => pNotaRI.lines(i).icms_amount_recover
                                                 , p_diff_icms_tax               => pNotaRI.lines(i).diff_icms_tax
                                                 , p_diff_icms_amount            => pNotaRI.lines(i).diff_icms_amount
                                                 , p_diff_icms_amount_recover    => pNotaRI.lines(i).diff_icms_amount_recover
                                                 , p_entity_id                   => l_cll_supp.entity_id
                                                 , p_ship_via_lookup_code        => NULL
                                                 , p_icms_type                   => pNotaRI.icms_type
                                                  );
             
             -- Se houver imposto a recuperar, define o CST para o tipo de imposto                        
             IF (NVL(pNotaRI.lines(i).diff_icms_amount_recover, 0) > 0) THEN
               pNotaRI.lines(i).tributary_status_code := NULL;
             END IF;
             
             CLL_F189_TAX_PKG.find_ipi(p_invoice_type_id         => pNotaRI.invoice_type_id --l_ml_params_rec.id_tipo_nf
                                     , p_business_id             => l_cll_supp.business_id
                                     , p_classification_id       => pNotaRI.lines(i).classification_id
                                     , p_utilization_id          => pNotaRI.lines(i).utilization_id
                                     , p_line_amount             => pNotaRI.gross_total_amount -- recebe total : l_total_invoice
                                     , p_dt_emissao              => TRUNC(SYSDATE)
                                     , p_ipi_tax                 => pNotaRI.lines(i).ipi_tax
                                     , p_ipi_amount              => pNotaRI.lines(i).ipi_amount
                                     , p_ipi_amount_recover      => pNotaRI.lines(i).ipi_amount_recover
                                     , p_ipi_tax_code            => pNotaRI.lines(i).ipi_tax_code
                                     , p_add_ipi_to_icms_flag    => l_add_ipi_to_icms_flag
                                     , p_quantity                => 1
                                     , p_ipi_unit_amount         => pNotaRI.lines(i).ipi_unit_amount
                                     , p_item_id                 => pNotaRI.lines(i).item_id
                                     , p_ipi_base_amount         => pNotaRI.lines(i).ipi_base_amount
                                      );
             
             -- Se houver imposto a recuperar, define o CST para o tipo de imposto
             IF NVL(pNotaRI.lines(i).ipi_amount_recover, 0) > 0 THEN
               pNotaRI.lines(i).ipi_tributary_code := NULL;
             END IF;             
             
             CLL_F189_TAX_PKG.find_pis(p_entity_id                    => l_cll_supp.entity_id
                                      ,p_organization_id              => pNotaRI.organization_id
                                      ,p_utilization_id               => pNotaRI.lines(i).utilization_id
                                      ,p_ipi_base_amount              => pNotaRI.gross_total_amount
                                      ,p_invoice_type_id              => pNotaRI.invoice_type_id
                                      ,p_classification_id            => pNotaRI.lines(i).classification_id
                                      ,p_pis_amount_recover           => pNotaRI.lines(i).pis_amount_recover
                                      ,p_ipi_amount                   => pNotaRI.lines(i).ipi_amount
                                      ,p_tax_rate                     => pNotaRI.lines(i).pis_tax_rate
                                      ,p_base_amount                  => l_base_amount_pis
                                      ,p_quantity                     => 1
                                      ,p_pis_unit_amount              => l_pis_unit_amount
                                      ,p_invoice_date                 => pNotaRI.invoice_date
                                      ,p_item_id                      => pNotaRI.lines(i).item_id
                                      );
                                      
             pNotaRI.lines(i).pis_base_amount := l_base_amount_pis;
                                      
             -- Se houver imposto a recuperar, define o CST para o tipo de imposto
             IF NVL(pNotaRI.lines(i).pis_amount_recover, 0) > 0 THEN
               pNotaRI.lines(i).pis_tributary_code := NULL;
             END IF;
             
             CLL_F189_TAX_PKG.Find_COFINS(p_entity_id                 => l_cll_supp.entity_id
                                         ,p_organization_id           => pNotaRI.organization_id
                                         ,p_utilization_id            => pNotaRI.lines(i).utilization_id
                                         ,p_ipi_base_amount           => pNotaRI.gross_total_amount
                                         ,p_invoice_type_id           => pNotaRI.invoice_type_id
                                         ,p_classification_id         => pNotaRI.lines(i).classification_id
                                         ,p_cofins_amount_recover     => pNotaRI.lines(i).cofins_amount_recover
                                         ,p_ipi_amount                => pNotaRI.lines(i).ipi_amount
                                         ,p_tax_rate                  => pNotaRI.lines(i).cofins_tax_rate
                                         ,p_base_amount               => l_base_amount_pis
                                         ,p_quantity                  => 1
                                         ,p_cofins_unit_amount        => l_cofins_unit_amount
                                         ,p_invoice_date              => pNotaRI.invoice_date
                                         ,p_item_id                   => pNotaRI.lines(i).item_id
                                         );
             
             -- Se houver imposto a recuperar, define o CST para o tipo de imposto      
             IF NVL(pNotaRI.lines(i).cofins_amount_recover,0) > 0 THEN
               pNotaRI.lines(i).cofins_tributary_code := NULL;
             END IF;
             
             IF (pSource NOT IN ('CONTA_ENERGIA', 'FREELANCER')) THEN
               -- Recupera o ID da combina��o cont�bil da linha do documento fiscal
               GET_EXPENSE_ACCOUNT(p_source             => pSource
                                  ,p_organization_id    => pNotaRI.organization_id
                                  ,p_item_id            => pNotaRI.lines(i).item_id
                                  ,p_cd_filial_contabil => pNotaRI.branch_office_num
                                  ,x_ccid               => pNotaRI.lines(i).db_code_combination_id
                                  ,p_ErrCode            => p_ErrCode
                                  ,p_cod_retorno        => p_cod_retorno
                                  ,p_msg_retorno        => p_msg_retorno);
                                  
               IF p_cod_retorno > 0 THEN
                 RAISE e_fatal_error;
               END IF;
             END IF;
          END LOOP;
        END IF;
        
    EXCEPTION
      WHEN e_fatal_error THEN
        p_cod_retorno  := 1;
      WHEN OTHERS THEN
        p_ErrCode      := '1150';
        p_cod_retorno  := 1;
        p_msg_retorno  := 'Falha Enriquecimento: '||SQLERRM;
    END ENRIQUECER_RI_NOTA;
    
    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: PROCEDURE PARA INSERT NA INTERFACE DO RI.                                               |
    |                                                                                                   |
    | PROCEDURE: INSERIR_RI_INTERFACE                                                                   |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 24-01-2017                 DIOGO ALCANTARA A. CATA PRETA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    PROCEDURE INSERIR_RI_INTERFACE(pNotaRI         IN OUT XXML_RI_NF_HEA_REC_TYPE,
                                   x_id_source        OUT VARCHAR2,
                                   p_ErrCode          OUT VARCHAR2,
                                   p_cod_retorno      OUT NUMBER,
                                   p_msg_retorno      OUT VARCHAR2) IS
                                   
        l_RI_header                 cll_f189_invoices_interface%ROWTYPE;
        l_RI_lines                  cll_f189_invoice_lines_iface%ROWTYPE;

        l_interface_invoice_id      NUMBER;
        l_interface_operation_id    NUMBER;
        l_id_docto_integracao       NUMBER;
        l_iface_invoice_line_id     NUMBER;

    BEGIN
        -- ---------------------------------------------------------------------
        -- SEQUENCES / IDS
        -- ---------------------------------------------------------------------
        SELECT cll_f189_invoices_interface_s.NEXTVAL
          INTO l_interface_invoice_id
          FROM dual;

        SELECT cll_f189_interface_operat_s.NEXTVAL
          INTO l_interface_operation_id
          FROM dual;

        -- ---------------------------------------------------------------------
        -- CABECALHO INTERFACE REGISTROS
        -- ---------------------------------------------------------------------
        l_RI_header.interface_invoice_id           := l_interface_invoice_id;
        l_RI_header.interface_operation_id         := l_interface_operation_id;
        l_RI_header.source                         := pNotaRI.source;
        l_RI_header.process_flag                   := 1;
        l_RI_header.gl_date                        := TRUNC(pNotaRI.gl_date);
        l_RI_header.freight_flag                   := 'N';
        l_RI_header.total_freight_weight           := pNotaRI.total_freight_weight;
        l_RI_header.invoice_id                     := pNotaRI.invoice_id;
        l_RI_header.entity_id                      := pNotaRI.entity_id;
        l_RI_header.document_type                  := pNotaRI.document_type;
        l_RI_header.document_number                := pNotaRI.document_number;
        l_RI_header.invoice_num                    := pNotaRI.invoice_num;
        l_RI_header.series                         := pNotaRI.series;
        l_RI_header.operation_id                   := pNotaRI.operation_id;
        l_RI_header.organization_id                := pNotaRI.organization_id;
        l_RI_header.organization_code              := pNotaRI.organization_code;
        l_RI_header.location_id                    := pNotaRI.location_id;
        l_RI_header.location_code                  := pNotaRI.location_code;
        l_RI_header.invoice_amount                 := pNotaRI.invoice_amount;
        l_RI_header.invoice_date                   := TRUNC(pNotaRI.invoice_date);
        l_RI_header.invoice_type_id                := pNotaRI.invoice_type_id;
        l_RI_header.invoice_type_code              := pNotaRI.invoice_type_code;
        l_RI_header.icms_type                      := pNotaRI.icms_type;
        l_RI_header.icms_base                      := ROUND(pNotaRI.icms_base, 2);
        l_RI_header.icms_tax                       := pNotaRI.icms_tax;
        l_RI_header.icms_amount                    := ROUND(pNotaRI.icms_amount, 2);
        l_RI_header.ipi_amount                     := pNotaRI.ipi_amount;
        l_RI_header.subst_icms_base                := pNotaRI.subst_icms_base;
        l_RI_header.subst_icms_amount              := pNotaRI.subst_icms_amount;
        l_RI_header.diff_icms_tax                  := pNotaRI.diff_icms_tax;
        l_RI_header.diff_icms_amount               := pNotaRI.diff_icms_amount;
        l_RI_header.iss_base                       := pNotaRI.iss_base;
        l_RI_header.iss_tax                        := pNotaRI.iss_tax;
        l_RI_header.iss_amount                     := pNotaRI.iss_amount;
        l_RI_header.ir_base                        := pNotaRI.ir_base;
        l_RI_header.ir_tax                         := pNotaRI.ir_tax;
        l_RI_header.ir_amount                      := pNotaRI.ir_amount;
        l_RI_header.description                    := pNotaRI.description;
        l_RI_header.terms_id                       := pNotaRI.terms_id;
        l_RI_header.terms_name                     := pNotaRI.terms_name;
        l_RI_header.terms_date                     := pNotaRI.terms_date;
        l_RI_header.first_payment_date             := pNotaRI.first_payment_date;
        l_RI_header.insurance_amount               := pNotaRI.insurance_amount;
        l_RI_header.freight_amount                 := pNotaRI.freight_amount;
        l_RI_header.payment_discount               := pNotaRI.payment_discount;
        l_RI_header.return_cfo_id                  := pNotaRI.return_cfo_id;
        l_RI_header.return_cfo_code                := pNotaRI.return_cfo_code;
        l_RI_header.return_amount                  := pNotaRI.return_amount;
        l_RI_header.return_date                    := pNotaRI.return_date;
        l_RI_header.additional_tax                 := pNotaRI.additional_tax;
        l_RI_header.additional_amount              := pNotaRI.additional_amount;
        l_RI_header.other_expenses                 := pNotaRI.other_expenses;
        l_RI_header.invoice_weight                 := pNotaRI.invoice_weight;
        l_RI_header.contract_id                    := pNotaRI.contract_id;
        l_RI_header.dollar_invoice_amount          := pNotaRI.dollar_invoice_amount;
        l_RI_header.source_items                   := pNotaRI.source_items;
        l_RI_header.importation_number             := pNotaRI.importation_number;
        l_RI_header.po_conversion_rate             := pNotaRI.po_conversion_rate;
        l_RI_header.importation_freight_weight     := pNotaRI.importation_freight_weight;
        l_RI_header.total_fob_amount               := pNotaRI.total_fob_amount;
        l_RI_header.freight_international          := pNotaRI.freight_international;
        l_RI_header.importation_tax_amount         := pNotaRI.importation_tax_amount;
        l_RI_header.importation_insurance_amount   := pNotaRI.importation_insurance_amount;
        l_RI_header.total_cif_amount               := pNotaRI.total_cif_amount;
        l_RI_header.customs_expense_func           := pNotaRI.customs_expense_func;
        l_RI_header.importation_expense_func       := pNotaRI.importation_expense_func;
        l_RI_header.dollar_total_fob_amount        := pNotaRI.dollar_total_fob_amount;
        l_RI_header.dollar_customs_expense         := pNotaRI.dollar_customs_expense;
        l_RI_header.dollar_freight_international   := pNotaRI.dollar_freight_international;
        l_RI_header.dollar_importation_tax_amount  := pNotaRI.dollar_importation_tax_amount;
        l_RI_header.dollar_insurance_amount        := pNotaRI.dollar_insurance_amount;
        l_RI_header.dollar_total_cif_amount        := pNotaRI.dollar_total_cif_amount;
        l_RI_header.importation_expense_dol        := pNotaRI.importation_expense_dol;
        l_RI_header.fiscal_document_model          := pNotaRI.fiscal_document_model;
        l_RI_header.irrf_base_date                 := pNotaRI.irrf_base_date;
        l_RI_header.inss_base                      := pNotaRI.inss_base;
        l_RI_header.inss_tax                       := pNotaRI.inss_tax;
        l_RI_header.inss_amount                    := pNotaRI.inss_amount;
        l_RI_header.ir_vendor                      := pNotaRI.ir_vendor;
        l_RI_header.ir_categ                       := pNotaRI.ir_categ;
        l_RI_header.icms_st_base                   := pNotaRI.icms_st_base;
        l_RI_header.icms_st_amount                 := pNotaRI.icms_st_amount;
        l_RI_header.icms_st_amount_recover         := pNotaRI.icms_st_amount_recover;
        l_RI_header.diff_icms_amount_recover       := pNotaRI.diff_icms_amount_recover;
        l_RI_header.alternate_currency_conv_rate   := pNotaRI.alternate_currency_conv_rate;
        l_RI_header.gross_total_amount             := pNotaRI.gross_total_amount;
        l_RI_header.source_state_id                := pNotaRI.source_state_id;
        l_RI_header.destination_state_id           := pNotaRI.destination_state_id;
        l_RI_header.source_state_code              := pNotaRI.source_state_code;
        l_RI_header.destination_state_code         := pNotaRI.destination_state_code;
        l_RI_header.funrural_base                  := pNotaRI.funrural_base;
        l_RI_header.funrural_tax                   := pNotaRI.funrural_tax;
        l_RI_header.funrural_amount                := pNotaRI.funrural_amount;
        l_RI_header.sest_senat_base                := pNotaRI.sest_senat_base;
        l_RI_header.sest_senat_tax                 := pNotaRI.sest_senat_tax;
        l_RI_header.sest_senat_amount              := pNotaRI.sest_senat_amount;
        l_RI_header.user_defined_conversion_rate   := pNotaRI.user_defined_conversion_rate;
        l_RI_header.po_currency_code               := pNotaRI.po_currency_code;
        l_RI_header.inss_autonomous_invoiced_total := pNotaRI.inss_autonomous_invoiced_total;
        l_RI_header.inss_autonomous_amount         := pNotaRI.inss_autonomous_amount;
        l_RI_header.inss_autonomous_tax            := pNotaRI.inss_autonomous_tax;
        l_RI_header.inss_additional_tax_1          := pNotaRI.inss_additional_tax_1;
        l_RI_header.inss_additional_tax_2          := pNotaRI.inss_additional_tax_2;
        l_RI_header.inss_additional_tax_3          := pNotaRI.inss_additional_tax_3;
        l_RI_header.inss_additional_base_1         := pNotaRI.inss_additional_base_1;
        l_RI_header.inss_additional_base_2         := pNotaRI.inss_additional_base_2;
        l_RI_header.inss_additional_base_3         := pNotaRI.inss_additional_base_3;
        l_RI_header.inss_additional_amount_1       := pNotaRI.inss_additional_amount_1;
        l_RI_header.inss_additional_amount_2       := pNotaRI.inss_additional_amount_2;
        l_RI_header.inss_additional_amount_3       := pNotaRI.inss_additional_amount_3;
        l_RI_header.iss_city_id                    := pNotaRI.iss_city_id;
        l_RI_header.siscomex_amount                := pNotaRI.siscomex_amount;
        l_RI_header.dollar_siscomex_amount         := pNotaRI.dollar_siscomex_amount;
        l_RI_header.ship_via_lookup_code           := pNotaRI.ship_via_lookup_code;
        l_RI_header.iss_city_code                  := pNotaRI.iss_city_code;
        l_RI_header.receive_date                   := NVL(pNotaRI.receive_date, TRUNC(SYSDATE));
        l_RI_header.freight_ap_flag                := pNotaRI.freight_ap_flag;
        l_RI_header.importation_pis_amount         := pNotaRI.importation_pis_amount;
        l_RI_header.importation_cofins_amount      := pNotaRI.importation_cofins_amount;
        l_RI_header.dollar_importation_pis_amount  := pNotaRI.dollar_importation_pis_amount;
        l_RI_header.dollar_import_cofins_amount    := pNotaRI.dollar_import_cofins_amount;
        l_RI_header.income_code                    := pNotaRI.income_code;
        l_RI_header.ie                             := pNotaRI.ie;
        l_RI_header.presumed_icms_tax_amount       := pNotaRI.presumed_icms_tax_amount;
        l_RI_header.creation_date                  := SYSDATE;
        l_RI_header.created_by                     := pNotaRI.created_by;
        l_RI_header.last_update_date               := SYSDATE;
        l_RI_header.last_updated_by                := pNotaRI.last_updated_by;
        l_RI_header.last_update_login              := pNotaRI.last_update_login;
        l_RI_header.request_id                     := pNotaRI.request_id;
        l_RI_header.program_application_id         := pNotaRI.program_application_id;
        l_RI_header.program_id                     := pNotaRI.program_id;
        l_RI_header.program_update_date            := pNotaRI.program_update_date;
        l_RI_header.ceo_attribute_category         := pNotaRI.ceo_attribute_category;
        l_RI_header.ceo_attribute1                 := pNotaRI.ceo_attribute1;
        l_RI_header.ceo_attribute2                 := pNotaRI.ceo_attribute2;
        l_RI_header.ceo_attribute3                 := pNotaRI.ceo_attribute3;
        l_RI_header.ceo_attribute4                 := pNotaRI.ceo_attribute4;
        l_RI_header.ceo_attribute5                 := pNotaRI.ceo_attribute5;
        l_RI_header.ceo_attribute6                 := pNotaRI.ceo_attribute6;
        l_RI_header.ceo_attribute7                 := pNotaRI.ceo_attribute7;
        l_RI_header.ceo_attribute8                 := pNotaRI.ceo_attribute8;
        l_RI_header.ceo_attribute9                 := pNotaRI.ceo_attribute9;
        l_RI_header.ceo_attribute10                := pNotaRI.ceo_attribute10;
        l_RI_header.ceo_attribute11                := pNotaRI.ceo_attribute11;
        l_RI_header.ceo_attribute12                := pNotaRI.ceo_attribute12;
        l_RI_header.ceo_attribute13                := pNotaRI.ceo_attribute13;
        l_RI_header.ceo_attribute14                := pNotaRI.ceo_attribute14;
        l_RI_header.ceo_attribute15                := pNotaRI.ceo_attribute15;
        l_RI_header.ceo_attribute16                := pNotaRI.ceo_attribute16;
        l_RI_header.ceo_attribute17                := pNotaRI.ceo_attribute17;
        l_RI_header.ceo_attribute18                := pNotaRI.ceo_attribute18;
        l_RI_header.ceo_attribute19                := pNotaRI.ceo_attribute19;
        l_RI_header.ceo_attribute20                := pNotaRI.ceo_attribute20;
        l_RI_header.cin_attribute_category         := pNotaRI.cin_attribute_category;
        l_RI_header.cin_attribute1                 := pNotaRI.cin_attribute1;
        l_RI_header.cin_attribute2                 := pNotaRI.cin_attribute2;
        l_RI_header.cin_attribute3                 := pNotaRI.cin_attribute3;
        l_RI_header.cin_attribute4                 := pNotaRI.cin_attribute4;
        l_RI_header.cin_attribute5                 := pNotaRI.cin_attribute5;
        l_RI_header.cin_attribute6                 := pNotaRI.cin_attribute6;
        l_RI_header.cin_attribute7                 := pNotaRI.cin_attribute7;
        l_RI_header.cin_attribute8                 := pNotaRI.cin_attribute8;
        l_RI_header.cin_attribute9                 := pNotaRI.cin_attribute9;
        l_RI_header.cin_attribute10                := pNotaRI.cin_attribute10;
        l_RI_header.cin_attribute11                := pNotaRI.cin_attribute11;
        l_RI_header.cin_attribute12                := pNotaRI.cin_attribute12;
        l_RI_header.cin_attribute13                := pNotaRI.cin_attribute13;
        l_RI_header.cin_attribute14                := pNotaRI.cin_attribute14;
        l_RI_header.ceo_attribute18                := pNotaRI.ceo_attribute18;
        l_RI_header.cin_attribute15                := pNotaRI.cin_attribute15;
        l_RI_header.cin_attribute16                := pNotaRI.cin_attribute16;
        l_RI_header.cin_attribute17                := pNotaRI.cin_attribute17;
        l_RI_header.cin_attribute18                := pNotaRI.cin_attribute18;
        l_RI_header.cin_attribute19                := pNotaRI.cin_attribute19;
        l_RI_header.cin_attribute20                := pNotaRI.cin_attribute20;
        l_RI_header.di_date                        := pNotaRI.di_date;
        l_RI_header.clearance_date                 := pNotaRI.clearance_date;
        l_RI_header.comments                       := pNotaRI.comments;
        l_RI_header.vehicle_oper_type              := pNotaRI.vehicle_oper_type;
        l_RI_header.vehicle_seller_doc_number      := pNotaRI.vehicle_seller_doc_number;
        l_RI_header.vehicle_seller_state_id        := pNotaRI.vehicle_seller_state_id;
        l_RI_header.vehicle_seller_state_code      := pNotaRI.vehicle_seller_state_code;
        l_RI_header.vehicle_chassi                 := pNotaRI.vehicle_chassi;
        l_RI_header.third_party_amount             := pNotaRI.third_party_amount;
        l_RI_header.abatement_amount               := pNotaRI.abatement_amount;
        l_RI_header.import_document_type           := pNotaRI.import_document_type;
        l_RI_header.eletronic_invoice_key          := pNotaRI.eletronic_invoice_key;
        l_RI_header.process_indicator              := pNotaRI.process_indicator;
        l_RI_header.process_origin                 := pNotaRI.process_origin;
        l_RI_header.subseries                      := pNotaRI.subseries;
        l_RI_header.icms_free_service_amount       := pNotaRI.icms_free_service_amount;
        l_RI_header.vendor_id                      := pNotaRI.vendor_id;
        l_RI_header.vendor_site_id                 := pNotaRI.vendor_site_id;
        l_RI_header.return_invoice_num             := pNotaRI.return_invoice_num;
        l_RI_header.return_series                  := pNotaRI.return_series;
        l_RI_header.inss_subcontract_amount        := pNotaRI.inss_subcontract_amount;
        l_RI_header.invoice_parent_id              := pNotaRI.invoice_parent_id;
        l_RI_header.service_execution_date         := pNotaRI.service_execution_date;
        l_RI_header.pis_withhold_amount            := pNotaRI.pis_withhold_amount;
        l_RI_header.cofins_withhold_amount         := pNotaRI.cofins_withhold_amount;
        l_RI_header.drawback_granted_act_number    := pNotaRI.drawback_granted_act_number;
        l_RI_header.ref_cte_number                 := pNotaRI.ref_cte_number;
        l_RI_header.cte_type                       := pNotaRI.cte_type;
        l_RI_header.cte_invoice_id                 := pNotaRI.cte_invoice_id;
        l_RI_header.cte_invoice_type               := pNotaRI.cte_invoice_type;
        l_RI_header.ceo_attribute5                 := pNotaRI.ceo_attribute5;
        l_RI_header.max_icms_amount_recover        := pNotaRI.max_icms_amount_recover;
        l_RI_header.icms_tax_rec_simpl_br          := pNotaRI.icms_tax_rec_simpl_br;
        l_RI_header.simplified_br_tax_flag         := pNotaRI.simplified_br_tax_flag;

        INSERT INTO cll_f189_invoices_interface VALUES l_RI_header;

        IF  pNotaRI.LINES IS NOT NULL AND pNotaRI.LINES.COUNT > 0 THEN
            FOR i IN pNotaRI.LINES.first .. pNotaRI.LINES.last LOOP

                -- -------------------------------------------------------------
                -- SEQUENCE LINHAS
                -- -------------------------------------------------------------
                SELECT cll_f189_invoice_lines_iface_s.NEXTVAL
                  INTO l_iface_invoice_line_id
                  FROM dual;

                -- -------------------------------------------------------------
                -- LINHA INTERFACE REGISTROS
                -- -------------------------------------------------------------
                l_RI_lines.interface_invoice_line_id      := l_iface_invoice_line_id;
                l_RI_lines.interface_invoice_id           := l_interface_invoice_id;
                l_RI_lines.line_location_id               := pNotaRI.LINES(i).line_location_id;
                l_RI_lines.requisition_line_id            := pNotaRI.LINES(i).requisition_line_id;
                l_RI_lines.item_id                        := pNotaRI.LINES(i).item_id;
                l_RI_lines.db_code_combination_id         := pNotaRI.LINES(i).db_code_combination_id;
                l_RI_lines.classification_id              := pNotaRI.LINES(i).classification_id;
                l_RI_lines.classification_code            := pNotaRI.LINES(i).classification_code;
                l_RI_lines.utilization_id                 := pNotaRI.LINES(i).utilization_id;
                l_RI_lines.utilization_code               := pNotaRI.LINES(i).utilization_code;
                l_RI_lines.cfo_id                         := pNotaRI.LINES(i).cfo_id;
                l_RI_lines.cfo_code                       := pNotaRI.LINES(i).cfo_code;
                l_RI_lines.uom                            := pNotaRI.LINES(i).uom;
                l_RI_lines.quantity                       := NVL(pNotaRI.LINES(i).quantity, 1);
                l_RI_lines.unit_price                     := pNotaRI.LINES(i).unit_price;
                l_RI_lines.operation_fiscal_type          := pNotaRI.LINES(i).operation_fiscal_type;
                l_RI_lines.description                    := pNotaRI.LINES(i).description;
                l_RI_lines.icms_base                      := NVL(pNotaRI.LINES(i).icms_base, 0);
                l_RI_lines.icms_tax                       := pNotaRI.LINES(i).icms_tax;
                l_RI_lines.icms_amount                    := pNotaRI.LINES(i).icms_amount;
                l_RI_lines.icms_amount_recover            := ROUND(pNotaRI.LINES(i).icms_amount_recover, 2);
                l_RI_lines.icms_tax_code                  := pNotaRI.LINES(i).icms_tax_code;
                l_RI_lines.diff_icms_tax                  := pNotaRI.LINES(i).diff_icms_tax;
                l_RI_lines.diff_icms_amount               := ROUND(pNotaRI.LINES(i).diff_icms_amount, 2);
                l_RI_lines.ipi_base_amount                := ROUND(NVL(pNotaRI.LINES(i).ipi_base_amount, 0), 2);
                l_RI_lines.ipi_tax                        := pNotaRI.LINES(i).ipi_tax;
                l_RI_lines.ipi_amount                     := ROUND(NVL(pNotaRI.LINES(i).ipi_amount, 0), 2);
                l_RI_lines.ipi_amount_recover             := ROUND(pNotaRI.LINES(i).ipi_amount_recover, 2);
                l_RI_lines.ipi_tax_code                   := pNotaRI.LINES(i).ipi_tax_code;
                l_RI_lines.total_amount                   := pNotaRI.LINES(i).total_amount;
                l_RI_lines.receipt_flag                   := pNotaRI.LINES(i).receipt_flag;
                l_RI_lines.release_tax_hold_reason        := pNotaRI.LINES(i).release_tax_hold_reason;
                l_RI_lines.tax_hold_released_by           := pNotaRI.LINES(i).tax_hold_released_by;
                l_RI_lines.func_prepayment_amount         := pNotaRI.LINES(i).func_prepayment_amount;
                l_RI_lines.dollar_prepayment_amount       := pNotaRI.LINES(i).dollar_prepayment_amount;
                l_RI_lines.fob_amount                     := pNotaRI.LINES(i).fob_amount;
                l_RI_lines.freight_internacional          := pNotaRI.LINES(i).freight_internacional;
                l_RI_lines.importation_tax_amount         := pNotaRI.LINES(i).importation_tax_amount;
                l_RI_lines.importation_insurance_amount   := pNotaRI.LINES(i).importation_insurance_amount;
                l_RI_lines.customs_expense_func           := pNotaRI.LINES(i).customs_expense_func;
                l_RI_lines.importation_expense_func       := pNotaRI.LINES(i).importation_expense_func;
                l_RI_lines.dollar_fob_amount              := pNotaRI.LINES(i).dollar_fob_amount;
                l_RI_lines.dollar_freight_internacional   := pNotaRI.LINES(i).dollar_freight_internacional;
                l_RI_lines.dollar_importation_tax_amount  := pNotaRI.LINES(i).dollar_importation_tax_amount;
                l_RI_lines.dollar_insurance_amount        := pNotaRI.LINES(i).dollar_insurance_amount;
                l_RI_lines.dollar_customs_expense         := pNotaRI.LINES(i).dollar_customs_expense;
                l_RI_lines.dollar_importation_expense     := pNotaRI.LINES(i).dollar_importation_expense;
                l_RI_lines.discount_percent               := pNotaRI.LINES(i).discount_percent;
                l_RI_lines.icms_st_base                   := pNotaRI.LINES(i).icms_st_base;
                l_RI_lines.icms_st_amount                 := pNotaRI.LINES(i).icms_st_amount;
                l_RI_lines.icms_st_amount_recover         := pNotaRI.LINES(i).icms_st_amount_recover;
                l_RI_lines.diff_icms_amount_recover       := ROUND(pNotaRI.LINES(i).diff_icms_amount_recover, 2);
                l_RI_lines.rma_interface_id               := pNotaRI.LINES(i).rma_interface_id;
                l_RI_lines.other_expenses                 := pNotaRI.LINES(i).other_expenses;
                l_RI_lines.discount_amount                := pNotaRI.LINES(i).discount_amount;
                l_RI_lines.freight_amount                 := pNotaRI.LINES(i).freight_amount;
                l_RI_lines.insurance_amount               := pNotaRI.LINES(i).insurance_amount;
                l_RI_lines.purchase_order_num             := pNotaRI.LINES(i).purchase_order_num;
                l_RI_lines.line_num                       := pNotaRI.LINES(i).line_num;
                l_RI_lines.shipment_num                   := pNotaRI.LINES(i).shipment_num;
                l_RI_lines.project_number                 := pNotaRI.LINES(i).project_number;
                l_RI_lines.project_id                     := pNotaRI.LINES(i).project_id;
                l_RI_lines.task_number                    := pNotaRI.LINES(i).task_number;
                l_RI_lines.task_id                        := pNotaRI.LINES(i).task_id;
                l_RI_lines.expenditure_type               := pNotaRI.LINES(i).expenditure_type;
                l_RI_lines.expenditure_organization_name  := pNotaRI.LINES(i).expenditure_organization_name;
                l_RI_lines.expenditure_organization_id    := pNotaRI.LINES(i).expenditure_organization_id;
                l_RI_lines.expenditure_item_date          := pNotaRI.LINES(i).expenditure_item_date;
                l_RI_lines.pis_amount_recover             := ROUND(pNotaRI.LINES(i).pis_amount_recover, 2);
                l_RI_lines.tributary_status_code          := pNotaRI.LINES(i).tributary_status_code;
                l_RI_lines.cofins_amount_recover          := ROUND(pNotaRI.LINES(i).cofins_amount_recover, 2);
                l_RI_lines.shipment_line_id               := pNotaRI.LINES(i).shipment_line_id;
                l_RI_lines.freight_ap_flag                := pNotaRI.LINES(i).freight_ap_flag;
                l_RI_lines.importation_pis_cofins_base    := pNotaRI.LINES(i).importation_pis_cofins_base;
                l_RI_lines.importation_pis_amount         := pNotaRI.LINES(i).importation_pis_amount;
                l_RI_lines.importation_cofins_amount      := pNotaRI.LINES(i).importation_cofins_amount;
                l_RI_lines.awt_group_id                   := pNotaRI.LINES(i).awt_group_id;
                l_RI_lines.presumed_icms_tax_base         := pNotaRI.LINES(i).presumed_icms_tax_base;
                l_RI_lines.presumed_icms_tax_perc         := pNotaRI.LINES(i).presumed_icms_tax_perc;
                l_RI_lines.presumed_icms_tax_amount       := pNotaRI.LINES(i).presumed_icms_tax_amount;
                l_RI_lines.creation_date                  := SYSDATE;
                l_RI_lines.created_by                     := pNotaRI.LINES(i).created_by;
                l_RI_lines.last_update_date               := SYSDATE;
                l_RI_lines.last_updated_by                := pNotaRI.LINES(i).last_updated_by;
                l_RI_lines.last_update_login              := pNotaRI.LINES(i).last_update_login;
                l_RI_lines.request_id                     := pNotaRI.LINES(i).request_id;
                l_RI_lines.program_application_id         := pNotaRI.LINES(i).program_application_id;
                l_RI_lines.program_id                     := pNotaRI.LINES(i).program_id;
                l_RI_lines.program_update_date            := pNotaRI.LINES(i).program_update_date;
                l_RI_lines.attribute_category             := pNotaRI.LINES(i).attribute_category;
                l_RI_lines.attribute1                     := pNotaRI.LINES(i).attribute1;
                l_RI_lines.attribute2                     := pNotaRI.LINES(i).attribute2;
                l_RI_lines.attribute3                     := pNotaRI.LINES(i).attribute3;
                l_RI_lines.attribute4                     := pNotaRI.LINES(i).attribute4;
                l_RI_lines.attribute5                     := pNotaRI.LINES(i).attribute5;
                l_RI_lines.attribute6                     := pNotaRI.LINES(i).attribute6;
                l_RI_lines.attribute7                     := pNotaRI.LINES(i).attribute7;
                l_RI_lines.attribute8                     := pNotaRI.LINES(i).attribute8;
                l_RI_lines.attribute9                     := pNotaRI.LINES(i).attribute9;
                l_RI_lines.attribute10                    := pNotaRI.LINES(i).attribute10;
                l_RI_lines.attribute11                    := pNotaRI.LINES(i).attribute11;
                l_RI_lines.attribute12                    := pNotaRI.LINES(i).attribute12;
                l_RI_lines.attribute13                    := pNotaRI.LINES(i).attribute13;
                l_RI_lines.attribute14                    := pNotaRI.LINES(i).attribute14;
                l_RI_lines.attribute15                    := pNotaRI.LINES(i).attribute15;
                l_RI_lines.attribute16                    := pNotaRI.LINES(i).attribute16;
                l_RI_lines.attribute17                    := pNotaRI.LINES(i).attribute17;
                l_RI_lines.attribute18                    := pNotaRI.LINES(i).attribute18;
                l_RI_lines.attribute19                    := pNotaRI.LINES(i).attribute19;
                l_RI_lines.attribute20                    := pNotaRI.LINES(i).attribute20;
                l_RI_lines.ipi_unit_amount                := ROUND(pNotaRI.LINES(i).ipi_unit_amount, 2);
                l_RI_lines.city_service_type_rel_id       := pNotaRI.LINES(i).city_service_type_rel_id;
                l_RI_lines.city_service_type_rel_code     := pNotaRI.LINES(i).city_service_type_rel_code;
                l_RI_lines.iss_base_amount                := pNotaRI.LINES(i).iss_base_amount;
                l_RI_lines.iss_tax_rate                   := pNotaRI.LINES(i).iss_tax_rate;
                l_RI_lines.iss_tax_amount                 := pNotaRI.LINES(i).iss_tax_amount;
                l_RI_lines.ipi_tributary_code             := pNotaRI.LINES(i).ipi_tributary_code;
                l_RI_lines.ipi_tributary_type             := pNotaRI.LINES(i).ipi_tributary_type;
                l_RI_lines.pis_base_amount                := ROUND(pNotaRI.LINES(i).pis_base_amount, 2);
                l_RI_lines.pis_tax_rate                   := pNotaRI.LINES(i).pis_tax_rate;
                l_RI_lines.pis_qty                        := pNotaRI.LINES(i).pis_qty;
                l_RI_lines.pis_unit_amount                := pNotaRI.LINES(i).pis_unit_amount;
                l_RI_lines.pis_amount                     := pNotaRI.LINES(i).pis_amount;
                l_RI_lines.pis_tributary_code             := pNotaRI.LINES(i).pis_tributary_code;
                l_RI_lines.cofins_base_amount             := pNotaRI.LINES(i).cofins_base_amount;
                l_RI_lines.cofins_tax_rate                := pNotaRI.LINES(i).cofins_tax_rate;
                l_RI_lines.cofins_qty                     := pNotaRI.LINES(i).cofins_qty;
                l_RI_lines.cofins_unit_amount             := pNotaRI.LINES(i).cofins_unit_amount;
                l_RI_lines.cofins_amount                  := pNotaRI.LINES(i).cofins_amount;
                l_RI_lines.cofins_tributary_code          := pNotaRI.LINES(i).cofins_tributary_code;
                l_RI_lines.exception_classification_code  := pNotaRI.LINES(i).exception_classification_code;
                l_RI_lines.deferred_icms_amount           := pNotaRI.LINES(i).deferred_icms_amount;
                l_RI_lines.net_amount                     := pNotaRI.LINES(i).net_amount;
                l_RI_lines.icms_base_reduc_perc           := pNotaRI.LINES(i).icms_base_reduc_perc;
                l_RI_lines.vehicle_oper_type              := pNotaRI.LINES(i).vehicle_oper_type;
                l_RI_lines.vehicle_chassi                 := pNotaRI.LINES(i).vehicle_chassi;
                l_RI_lines.uom_po                         := pNotaRI.LINES(i).uom_po;
                l_RI_lines.quantity_uom_po                := pNotaRI.LINES(i).quantity_uom_po;
                l_RI_lines.unit_price_uom_po              := pNotaRI.LINES(i).unit_price_uom_po;
                l_RI_lines.customs_total_value            := pNotaRI.LINES(i).customs_total_value;
                l_RI_lines.ci_percent                     := pNotaRI.LINES(i).ci_percent;
                l_RI_lines.total_import_parcel            := pNotaRI.LINES(i).total_import_parcel;
                l_RI_lines.fci_number                     := pNotaRI.LINES(i).fci_number;

                INSERT INTO cll_f189_invoice_lines_iface VALUES l_RI_lines;
            END LOOP;
        END IF;
        
        x_id_source   := pNotaRi.source;
        p_cod_retorno := 0;
        p_msg_retorno := 'SUCESSO';
    EXCEPTION
      WHEN OTHERS THEN
        p_ErrCode     := '3999';
        p_cod_retorno := 1;
        p_msg_retorno := 'Falha Insercao Interface: '||SQLERRM||CHR(10)||
                         'Localizacao: '||DBMS_UTILITY.format_error_backtrace;
    END INSERIR_RI_INTERFACE;

    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: PROCEDURE PARA CRIACAO DA NOTA NA INTERFACE DO RI.                                      |
    |                                                                                                   |
    | PROCEDURE: CRIAR_NOTA                                                                             |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 24-01-2017                 DIOGO ALCANTARA A. CATA PRETA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    PROCEDURE CRIAR_NOTA(pSource            IN VARCHAR2,
                         pUUID_Protocol     IN VARCHAR2,
                         pNotaRI            IN XXML_RI_NF_HEA_REC_TYPE,
                         pNeedApproval     OUT VARCHAR2,
                         pInterfaceRIId    OUT VARCHAR2,
                         pStatus           OUT VARCHAR2,
                         pHttpResponse     OUT NUMBER,
                         pErrCode          OUT VARCHAR2,
                         pErrBuffer        OUT VARCHAR2) IS
    
        vNotaRI                 XXML_RI_NF_HEA_REC_TYPE;
        vCodRetorno             NUMBER := 0;
        vMsgRetorno             VARCHAR2(4000);
        pReqHistory             XXML_OEBS_PROTOCOL_HISTORY%rowtype;
        v_result_hist           BOOLEAN;

        e_fatal_error           EXCEPTION;
        e_fatal_log_error       EXCEPTION;
    BEGIN

        pStatus         := 'S';
        pHttpResponse   := 200;

        vNotaRI         :=  pNotaRI;

        ------------------------------------------------------------------------
        -- ESTRUTURA / VALIDACAO DE HISTORICO
        ------------------------------------------------------------------------
        pReqHistory.uuid_protocol         := pUUID_Protocol;
        pReqHistory.action                := 'CREATION';
        pReqHistory.status                := 'SUCCESS';
        pReqHistory.attribute_category    := 'RI_INVOICE';
        pReqHistory.HTTPRESPONSE          := pHttpResponse;
        pReqHistory.attribute1            := pSource;

        IF TRIM(pUUID_Protocol) IS NOT NULL AND NOT(XXML_OEBS_UTIL_PKG.VALIDATE_UUID(pUUID_Protocol)) THEN
            vCodRetorno                   := 1;
            pErrCode                      := '92|';
            pHttpResponse                 := 400;
            pErrBuffer                    := 'O Protocolo informado n�o � v�lido.';

            RAISE e_fatal_log_error;
        ELSIF TRIM(pUUID_Protocol) IS NULL THEN
            vCodRetorno                   := 1;
            pErrCode                      := '33|';
            pHttpResponse                 := 400;
            pErrBuffer                    := 'Protocolo deve ser informado.';

            RAISE e_fatal_log_error;
        END IF;
        
        ------------------------------------------------------------------------
        -- VALIDACAO INICIAL
        ------------------------------------------------------------------------
        VALIDAR_RI_NOTA(pNotaRI       => vNotaRI
                       ,pSource       => pSource
                       ,p_ErrCode     => pErrCode                               -- 500 a 800
                       ,p_cod_retorno => vCodRetorno
                       ,p_msg_retorno => vMsgRetorno
                       );
                          
        IF (vCodRetorno > 0) THEN
          pErrBuffer := vMsgRetorno;
          GOTO set_errors;
        END IF;
        
        ------------------------------------------------------------------------
        -- ENRIQUECIMENTO DOS CAMPOS PARA INTERFACE
        ------------------------------------------------------------------------
        ENRIQUECER_RI_NOTA(pNotaRI         => vNotaRI
                          ,pSource         => pSource
                          ,pUUID_Protocol  => pUUID_Protocol
                          ,p_need_approval => pNeedApproval
                          ,p_ErrCode       => pErrCode                          -- 1000 a 2999
                          ,p_cod_retorno   => vCodRetorno
                          ,p_msg_retorno   => vMsgRetorno
                          );
                          
        IF (vCodRetorno > 0) THEN
          pErrBuffer := vMsgRetorno;
          GOTO set_errors;
        END IF;

        ------------------------------------------------------------------------
        -- INSERE INTERFACE
        ------------------------------------------------------------------------
        INSERIR_RI_INTERFACE(pNotaRI         => vNotaRI
                            ,x_id_source     => pInterfaceRIId
                            ,p_ErrCode       => pErrCode                        -- 3000 a 4999
                            ,p_cod_retorno   => vCodRetorno
                            ,p_msg_retorno   => vMsgRetorno
                            );
        
        IF (vCodRetorno > 0) THEN
          pErrBuffer := vMsgRetorno;
          GOTO set_errors;
        END IF;
        
        ------------------------------------------------------------------------
        -- HISTORICO DE ERROS
        ------------------------------------------------------------------------
        <<set_errors>>
        IF (vCodRetorno > 0) THEN
          pStatus                  := 'E';
          
          pReqHistory.status       :=  'ERROR';
          pReqHistory.HTTPRESPONSE :=  400;
          pReqHistory.ERRCODE      :=  pErrCode;
          pReqHistory.ERRBUFFER    :=  pErrBuffer;
          
          UPDATE_PROTOCOL_HISTORY(pReqHistory);
        END IF;
        
        pReqHistory.attribute3 := vNotaRI.interface_invoice_id;
        
        UPDATE_PROTOCOL_HISTORY(pReqHistory);

    EXCEPTION
      WHEN e_fatal_log_error THEN
        pStatus       := 'E';
      WHEN OTHERS THEN
        pStatus       := 'E';
        pHttpResponse := 500;
        pErrCode      := '5000';
        pErrBuffer    := 'Falha programa CRIAR RI: '||SQLERRM;
        
        UPDATE_PROTOCOL_HISTORY(pReqHistory);
    END CRIAR_NOTA;

    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: PROCEDURE PARA APROVAR AS NOTAS INCOMPLETAS DADOS ANTES DE INSERIR NA INTERFACE DO RI.  |
    |                                                                                                   |
    | PROCEDURE: APROVAR_RI_NOTA                                                                        |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 24-01-2017                 DIOGO ALCANTARA A. CATA PRETA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    |                                                                                                   |
    | UPDATE DATE                UPDATED BY                                                             |
    | --------------------------------------------------------------------------------------------------+
    | 12-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    PROCEDURE APROVAR_RI_NOTA(pOperationId        IN NUMBER,
                              pOrganizationId     IN NUMBER,
                              p_ErrCode          OUT VARCHAR2,
                              p_cod_retorno      OUT NUMBER,
                              p_msg_retorno      OUT VARCHAR2) IS
    
      v_status          VARCHAR(100);
      v_location_id     NUMBER;
      v_created_by      NUMBER;
      vLine_location_id NUMBER;
      vInvoice_id       NUMBER;
      vPo_header_id     NUMBER;
      
      l_request_id      NUMBER;
      l_status          VARCHAR2(100);
      l_message         VARCHAR2(250);
    
      var1              VARCHAR2(20);
      var2              VARCHAR2(20);
      var3              VARCHAR2(20);
      var4              VARCHAR2(20);
      var5              VARCHAR2(20);
      var6              VARCHAR2(20);
      var7              VARCHAR2(20);
      var8              DATE := SYSDATE;
      var9              NUMBER;
      var10             NUMBER;

      e_fatal_error     EXCEPTION;
    
      CURSOR c_cll_invoice_distr IS
        SELECT cfid.invoice_id,
               cfid.invoice_distrib_id,
               cfid.po_distribution_id,
               cfid.code_combination_id ,
               pda.code_combination_id  po_code_combination_id
          FROM cll_f189_invoices        cfi,
               cll_f189_invoice_dist    cfid,
               po_distributions_all     pda
         WHERE 1 = 1
           AND cfi.invoice_id            = cfid.invoice_id
           AND cfid.reference            = 'ITEM'
           AND cfid.po_distribution_id   = pda.po_distribution_id
           AND cfid.code_combination_id != pda.code_combination_id
           AND cfi.OPERATION_ID          = pOperationId
           AND cfi.ORGANIZATION_id       = pOrganizationId;
               
      CURSOR c_cll_distr IS
        SELECT cfd.distribution_id,
               cfd.code_combination_id,
               cfd.po_distribution_id,
               pda.code_combination_id  po_code_combination_id
          FROM cll_f189_distributions   cfd,
               po_distributions_all     pda
         WHERE 1 = 1
           AND cfd.reference            = 'ITEM'
           AND cfd.po_distribution_id   = pda.po_distribution_id
           AND cfd.code_combination_id != pda.code_combination_id
           AND cfd.operation_id         = pOperationId
           AND cfd.organization_id      = pOrganizationId;
    
    BEGIN
      p_cod_retorno := 0;
      
      BEGIN
        SELECT status,
               location_id,
               created_by
          INTO v_status,
               v_location_id,
               v_created_by
          FROM cll_f189_entry_operations
         WHERE operation_id    = pOperationId
           AND organization_id = pOrganizationId;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          p_ErrCode      := '210';
          p_msg_retorno  := 'Nota nao encontrada/criada';
          RAISE e_fatal_error;
      END;
         
      IF (v_status = 'INCOMPLETE') THEN
        ------------------------------------------------------------------------
        -- APIs PARA CALCULO DE TAXAS ESPECIFICAS
        ------------------------------------------------------------------------
        CLL_F189_CALC_IDX_DOLLAR_PKG.proc_calc_idx_dollar(pOperationId,v_location_id,pOrganizationId,var1,var2,var3,var4,var5,var6,var7);

        CLL_F189_TAX_PKG.find_icms_subst(pOrganizationId, v_location_id, pOperationId);

        CLL_F189_FREIGHT_PIS_PKG.proportional_values(pOperationId, pOrganizationId);

        CLL_F189_FREIGHT_COFINS.proportional_values(pOperationId, pOrganizationId);

        CLL_F189_TAX_PKG.find_icms_st(pOrganizationId, v_location_id, pOperationId);

        CLL_F189_FREIGHT_DIFFERENT_PKG.proc_freight_differential(pOperationId, v_location_id, pOrganizationId);

        CLL_F189_AVERAGE_COST_PKG.proc_average_cost(pOperationId, pOrganizationId, v_location_id);

        CLL_F189_CALC_IDX_DOLLAR_PKG.proc_calc_idx_dollar(pOperationId, v_location_id, pOrganizationId, var1, var2, var3, var4, var5, var6, var7);

        CLL_F189_POST_TO_GL_PKG.proc_post_to_gl(pOperationId, 81, pOrganizationId, var8, SYSDATE, v_created_by, var1, var9, var10);
      
        ------------------------------------------------------------------------
        -- ATUALIZACAO SEG. CONTABEIS NA DISTRIBUICAO / STATUS RECEBIMENTOS
        ------------------------------------------------------------------------
        FOR pc_cll_invoice_distr IN c_cll_invoice_distr LOOP
          UPDATE cll_f189_invoice_dist
             SET code_combination_id = pc_cll_invoice_distr.po_code_combination_id
           WHERE invoice_distrib_id  = pc_cll_invoice_distr.invoice_distrib_id
             AND po_distribution_id  = pc_cll_invoice_distr.po_distribution_id;
        END LOOP;
       
        FOR pc_cll_distr IN c_cll_distr LOOP
          UPDATE cll_f189_distributions
             SET code_combination_id = pc_cll_distr.po_code_combination_id
           WHERE distribution_id     = pc_cll_distr.distribution_id
             AND po_distribution_id  = pc_cll_distr.po_distribution_id;
        END LOOP;
          
        UPDATE cll_f189_entry_operations
           SET status = 'IN PROCESS'
         WHERE operation_id    = pOperationId
           AND organization_id = pOrganizationId;
           
        COMMIT;
          
      ELSE
        p_ErrCode      := '250';
        p_cod_retorno  := 1;
        p_msg_retorno  := 'Status nao estava incompleto para realizar aprovacao';
      END IF;
      
    EXCEPTION
      WHEN e_fatal_error THEN
        p_cod_retorno := 1;
      WHEN OTHERS THEN
        p_ErrCode     := '299';
        p_cod_retorno := 1;
        p_msg_retorno := 'Falha no programa: '||SQLERRM;
    END APROVAR_RI_NOTA;

END XXML_RI_INTERFACE_PKG;
/
EXIT
