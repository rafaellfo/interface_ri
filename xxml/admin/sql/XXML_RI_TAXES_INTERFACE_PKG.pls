CREATE OR REPLACE
PACKAGE XXML.XXML_RI_TAXES_INTERFACE_PKG AUTHID CURRENT_USER IS

  PROCEDURE CALCULATE_INVOICE_TAXES(p_organization_id                 IN CLL_F189_INVOICES.ORGANIZATION_ID%TYPE
                                   ,p_invoice_type_id                 IN CLL_F189_INVOICES.INVOICE_TYPE_ID%TYPE
                                   ,p_invoice_id                      IN CLL_F189_INVOICES.INVOICE_ID%TYPE
                                   ,p_gross_total_amount              IN CLL_F189_INVOICES.GROSS_TOTAL_AMOUNT%TYPE
                                   ,p_entity_id                       IN CLL_F189_INVOICES.ENTITY_ID%TYPE
                                   ,p_gl_date                         IN CLL_F189_ENTRY_OPERATIONS.GL_DATE%TYPE
                                   ,p_invoice_amount              IN OUT CLL_F189_INVOICES.INVOICE_AMOUNT%TYPE
                                   ,p_inss_base                      OUT CLL_F189_INVOICES.INSS_BASE%TYPE
                                   ,p_inss_autonomous_tax            OUT CLL_F189_INVOICES.INSS_AUTONOMOUS_TAX%TYPE
                                   ,p_inss_autonomous_amount         OUT CLL_F189_INVOICES.INSS_AUTONOMOUS_AMOUNT%TYPE
                                   ,p_inss_autonomous_invoiced_tot   OUT CLL_F189_INVOICES.INSS_AUTONOMOUS_INVOICED_TOTAL%TYPE
                                   ,p_inss_withhold_invoice_id       OUT CLL_F189_INVOICES.INSS_WITHHOLD_INVOICE_ID%TYPE
                                   ,p_inss_tax                       OUT CLL_F189_INVOICES.INSS_TAX%TYPE
                                   ,p_sest_senat_base                OUT CLL_F189_INVOICES.SEST_SENAT_BASE%TYPE
                                   ,p_sest_senat_tax                 OUT CLL_F189_INVOICES.SEST_SENAT_TAX%TYPE
                                   ,p_sest_senat_amount              OUT CLL_F189_INVOICES.SEST_SENAT_AMOUNT%TYPE);

  FUNCTION ICMS_TAX(p_source_state_id IN NUMBER
                   ,p_dest_state_id   IN NUMBER) RETURN NUMBER;
                     
  PROCEDURE FIND_ICMS(p_invoice_type_id          IN            NUMBER
                     ,p_source_state_id          IN            NUMBER
                     ,p_destination_state_id     IN            NUMBER
                     ,p_classification_id        IN            NUMBER
                     ,p_utilization_id           IN            NUMBER
                     ,p_line_amount              IN            NUMBER
                     ,p_ipi_amount               IN            NUMBER
                     ,p_add_ipi_to_icms_flag     IN            VARCHAR2
                     ,p_icms_tax                 IN OUT NOCOPY NUMBER
                     ,p_icms_base                   OUT NOCOPY NUMBER
                     ,p_icms_amount                 OUT NOCOPY NUMBER
                     ,p_icms_tax_code               OUT NOCOPY VARCHAR2
                     ,p_recover_icms_amount         OUT NOCOPY NUMBER
                     ,p_diff_icms_tax               OUT NOCOPY NUMBER
                     ,p_diff_icms_amount            OUT NOCOPY NUMBER
                     ,p_diff_icms_amount_recover    OUT NOCOPY NUMBER
                     ,p_entity_id                IN            NUMBER
                     ,p_ship_via_lookup_code     IN            VARCHAR2
                     ,p_icms_type                IN            VARCHAR2);
                     
END XXML_RI_TAXES_INTERFACE_PKG;
/

CREATE SYNONYM XXML_RI_TAXES_INTERFACE_PKG FOR XXML.XXML_RI_TAXES_INTERFACE_PKG
/
EXIT
