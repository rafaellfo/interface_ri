CREATE OR REPLACE 
PACKAGE XXML.XXML_FIN_INT_PARAMETER_PKG IS

  /* $Header: XXML_FIN_INT_PARAMETER_PKG_S.pls 120.0 2012/03/22 08:00:00 djeremias ship $ */
  --
  -- +========================================================================================+
  -- |                     Datainfo IT Solutions, Santa Catarina, Brasil                      |
  -- |                                 All rights reserved.                                   |
  -- +========================================================================================+
  -- | FILENAME                                                                               |
  -- | XXML_FIN_INT_PARAMETER_PKG_S.pls                                                       |
  -- |                                                                                        |
  -- | PURPOSE                                                                                |
  -- |  Rotinas auxiliares do processo de integra��o financeira.                              |
  -- |                                                                                        |
  -- | CREATED BY Daniel Jeremias         22-MAR-2012 08:00                                   |
  -- | Os coment�rios iniciados com /** s�o utilizados por um aplicativo interno (plsqldoc)   |
  -- | da Datainfo para a gera��o da documenta��o do programa em fomato HTML. Ver utilit�rio  |
  -- | em http://www.allroundautomations.com/plsplsqldoc.html.                                |
  -- |                                                                                        |
  -- | UPDATED BY Ricardo H. Gross        18-MAI-2012 17:27                                   |
  -- | Com a cria��o da tabela xxml_item_param_gerais, foram realializadas altera��es para    |
  -- | que a mesma seja atualizada junto com a sua tabela pai.                                |
  -- |                                                                                        |
  -- +========================================================================================+
  --

  /** Associa atributos da organiza��o informada � todas as organiza��es cadastradas e ativas
      C�digo implementado dentro do pacote e utilizado na aplica��o SUP_E_033
      devido a impossibilidade de compilar o c�digo no Oracle Forms.
    %param p_organization_id Identificador da organiza��o de origem
  */
  PROCEDURE Assign_Org_All (
    p_organization_id IN ORG_ORGANIZATION_DEFINITIONS.ORGANIZATION_ID%TYPE
  );

  /** Associa atributos da organiza��o de origem � organiza��o de destino
      C�digo implementado dentro do pacote e utilizado na aplica��o SUP_E_033
      devido a impossibilidade de compilar o c�digo no Oracle Forms.
    %param p_src_organization_id Identificador da organiza��o de origem
    %param p_dst_organization_id Identificador da organiza��o de destino
  */
  PROCEDURE Assign_Org (
    p_src_organization_id IN ORG_ORGANIZATION_DEFINITIONS.ORGANIZATION_ID%TYPE
  , p_dst_organization_id IN ORG_ORGANIZATION_DEFINITIONS.ORGANIZATION_ID%TYPE
  );

  /** Desassocia atributos da organiza��o
      %param p_organization_id Identificador da organiza��o de origem
  */
  PROCEDURE Deassign_Org (
    p_organization_id     IN ORG_ORGANIZATION_DEFINITIONS.ORGANIZATION_ID%TYPE
  );

  /** Desassocia todos os atributos da organiza��o exceto a organiza��o de origem
      %param p_organization_id Identificador da organiza��o de origem
  */
  PROCEDURE Deassign_Org_All (
    p_organization_id     IN ORG_ORGANIZATION_DEFINITIONS.ORGANIZATION_ID%TYPE
  );
  
  FUNCTION IS_GL_PERIOD_CLOSED(p_gl_date         IN DATE
                              ,p_organization_id IN NUMBER) RETURN BOOLEAN;
                              
  FUNCTION GET_SUPPLIER_SUFFIX(p_lookup_type IN VARCHAR2
                              ,p_source      IN VARCHAR2) RETURN VARCHAR2;
  
  FUNCTION GET_SUPPLIER_TYPE(p_cpf_cnpj IN VARCHAR2) RETURN VARCHAR2;
  
  FUNCTION GET_HEADER_PARAMS(p_source    IN VARCHAR2
                            ,p_supp_type IN VARCHAR2) RETURN xxml_param_headers%ROWTYPE;
  
  FUNCTION GET_LINES_PARAM(p_source    IN VARCHAR2
                          ,p_supp_type IN VARCHAR2) RETURN VARCHAR2;

END XXML_FIN_INT_PARAMETER_PKG;
/

CREATE SYNONYM XXML_FIN_INT_PARAMETER_PKG FOR XXML.XXML_FIN_INT_PARAMETER_PKG
/
EXIT
