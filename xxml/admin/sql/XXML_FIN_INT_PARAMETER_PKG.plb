CREATE OR REPLACE 
PACKAGE BODY XXML.XXML_FIN_INT_PARAMETER_PKG IS

  /* $Header: XXML_FIN_INT_PARAMETER_PKG_B.plb 120.0 2012/03/22 08:00:00 djeremias ship $ */
  --
  -- +========================================================================================+
  -- |                     Datainfo IT Solutions, Santa Catarina, Brasil                      |
  -- |                                 All rights reserved.                                   |
  -- +========================================================================================+
  -- | FILENAME                                                                               |
  -- | XXML_FIN_INT_PARAMETER_PKG_B.plb                                                       |
  -- |                                                                                        |
  -- | PURPOSE                                                                                |
  -- |  Rotinas auxiliares da tela SUP_E_033.                                                 |
  -- |                                                                                        |
  -- | CREATED BY Daniel Jeremias         23-MAR-2012 10:30                                   |
  -- | Os coment�rios iniciados com /** s�o utilizados por um aplicativo interno (plsqldoc)   |
  -- | da Datainfo para a gera��o da documenta��o do programa em fomato HTML. Ver utilit�rio  |
  -- | em http://www.allroundautomations.com/plsplsqldoc.html.                                |
  -- |                                                                                        |
  -- | UPDATED BY                                                                             |
  -- |      Rafael Luiz de Faria Oliveira 17-JUL-2017                                         |
  -- | Criado novas funcoes e procedimentos para atender novo programa de interface do RI.    |
  -- | Incluido procedimentos para uso em futura tela OAF para manutencao de dados para       |
  -- | derivacoes de valores em integracoes.                                                  |
  -- |                                                                                        |
  -- | UPDATED BY                                                                             |
  -- |      Ricardo H. Gross              18-MAI-2012 17:27                                   |
  -- | Com a cria��o da tabela xxml_item_param_gerais, foram realializadas altera��es para    |
  -- | que a mesma seja atualizada junto com a sua tabela pai.                                |
  -- |                                                                                        |
  -- +========================================================================================+
  --

  /** Associa atributos da organiza��o informada � todas as organiza��es cadastradas e ativas
      C�digo implementado dentro do pacote e utilizado na aplica��o SUP_E_033
      devido a impossibilidade de compilar o c�digo no Oracle Forms.
    %param p_organization_id Identificador da organiza��o de origem
  */
  PROCEDURE Assign_Org_All (
    p_organization_id     IN ORG_ORGANIZATION_DEFINITIONS.ORGANIZATION_ID%TYPE
  ) IS
    l_sysdate  DATE   DEFAULT SYSDATE;
    l_user_id  NUMBER DEFAULT FND_GLOBAL.User_Id;
    l_login_id NUMBER DEFAULT FND_GLOBAL.Login_Id;
  BEGIN

    -- Copia as informa��es da organiza��o de origem para a organiza��o de destino
    INSERT INTO xxml_param_gerais_integracao
    SELECT orgs.organization_id id_organizacao_inventario
         , parm.id_tipo_integracao
         , parm.id_tipo_fornec
         , parm.id_condicao_pagamento
         , parm.cd_documento_fiscal
         , parm.ds_serie_nf
                 -- Recupera ID do tipo de nota na organiza��o de destino
         , NVL((SELECT dst.invoice_type_id
                  FROM cll_f189_invoice_types src
                     , cll_f189_invoice_types dst
                 WHERE src.invoice_type_id   = parm.id_tipo_nf
                   AND src.organization_id   = parm.id_organizacao_inventario
                   AND dst.organization_id   = orgs.organization_id
                   AND dst.invoice_type_code = src.invoice_type_code), parm.id_tipo_nf) id_tipo_nf
         , parm.cd_pagamento_gps
         , l_user_id  created_by
         , l_sysdate  creation_date
         , l_user_id  last_updated_by
         , l_sysdate  last_update_date
         , l_login_id last_update_login
      FROM xxml_param_gerais_integracao   parm
         , org_organization_definitions   orgs
     WHERE parm.id_organizacao_inventario = p_organization_id
       AND orgs.organization_id          != parm.id_organizacao_inventario;

    -- Copia as informa��es dos itens da organiza��o de origem para a organiza��o de destino.
    INSERT INTO xxml_item_param_gerais
    SELECT organization_id id_organizacao_inventario
         , id_tipo_integracao
         , id_tipo_fornec
         , id_item
         , l_user_id   created_by
         , l_sysdate   creation_date
         , l_user_id   last_updated_by
         , l_sysdate   last_update_date
         , l_login_id  last_update_login
      FROM xxml_item_param_gerais         parm
         , org_organization_definitions   orgs
     WHERE parm.id_organizacao_inventario = p_organization_id
       AND orgs.organization_id          != parm.id_organizacao_inventario;

  END Assign_Org_All;

  /** Associa atributos da organiza��o de origem � organiza��o de destino
      C�digo implementado dentro do pacote e utilizado na aplica��o SUP_E_033
      devido a impossibilidade de compilar o c�digo no Oracle Forms.
    %param p_src_organization_id Identificador da organiza��o de origem
    %param p_dst_organization_id Identificador da organiza��o de destino
  */
  PROCEDURE Assign_Org (
    p_src_organization_id IN ORG_ORGANIZATION_DEFINITIONS.ORGANIZATION_ID%TYPE
  , p_dst_organization_id IN ORG_ORGANIZATION_DEFINITIONS.ORGANIZATION_ID%TYPE
  ) IS
    l_sysdate  DATE   DEFAULT SYSDATE;
    l_user_id  NUMBER DEFAULT FND_GLOBAL.User_Id;
    l_login_id NUMBER DEFAULT FND_GLOBAL.Login_Id;
  BEGIN

    -- Copia as informa��es da organiza��o de origem para a organiza��o de destino
    INSERT INTO xxml_param_gerais_integracao
    SELECT p_dst_organization_id id_organizacao_inventario
                 , id_tipo_integracao
                 , id_tipo_fornec
                 , id_condicao_pagamento
                 , cd_documento_fiscal
                 , ds_serie_nf
                 -- Recupera ID do tipo de nota na organiza��o de destino
         , NVL((SELECT dst.invoice_type_id
                  FROM cll_f189_invoice_types src
                     , cll_f189_invoice_types dst
                 WHERE src.invoice_type_id   = id_tipo_nf
                   AND src.organization_id   = id_organizacao_inventario
                   AND dst.organization_id   = p_dst_organization_id
                   AND dst.invoice_type_code = src.invoice_type_code), id_tipo_nf) id_tipo_nf
                 , cd_pagamento_gps
                 , l_user_id  created_by
         , l_sysdate  creation_date
         , l_user_id  last_updated_by
         , l_sysdate  last_update_date
         , l_login_id last_update_login
      FROM xxml_param_gerais_integracao prms
     WHERE id_organizacao_inventario = p_src_organization_id;

    -- Copia as informa��es dos itens da organiza��o de origem para a organiza��o de destino.
    INSERT INTO xxml_item_param_gerais
    SELECT p_dst_organization_id id_organizacao_inventario
         , id_tipo_integracao
         , id_tipo_fornec
         , id_item
         , l_user_id   created_by
         , l_sysdate   creation_date
         , l_user_id   last_updated_by
         , l_sysdate   last_update_date
         , l_login_id  last_update_login
     FROM xxml_item_param_gerais ipg
    WHERE ipg.id_organizacao_inventario = p_src_organization_id;

  END Assign_Org;

  /** Desassocia atributos da organiza��o
      %param p_organization_id Identificador da organiza��o de origem
  */
  PROCEDURE Deassign_Org(p_organization_id IN ORG_ORGANIZATION_DEFINITIONS.ORGANIZATION_ID%TYPE) IS
  BEGIN
    -- Exclui registros para a organiza��o informada
    -- Os filhos s�o exclu�dos pela FOREIGN KEY criada
    -- com ON DELETE CASCADE.
    DELETE xxml_param_gerais_integracao
     WHERE id_organizacao_inventario = p_organization_id;

  END Deassign_Org;

  /** Desassocia todos os atributos da organiza��o exceto a organiza��o de origem
      %param p_organization_id Identificador da organiza��o de origem
  */
  PROCEDURE Deassign_Org_All(p_organization_id IN ORG_ORGANIZATION_DEFINITIONS.ORGANIZATION_ID%TYPE) IS
  BEGIN
    -- Exclui registros para a organiza��o informada
    -- Os filhos s�o exclu�dos pela FOREIGN KEY criada
    -- com ON DELETE CASCADE.
    DELETE xxml_param_gerais_integracao
     WHERE id_organizacao_inventario != p_organization_id;

  END Deassign_Org_All;
  
    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: VERIFICA SE PERIODO GL ESTA ABERTO OU FECHADO                                           |
    |                                                                                                   |
    | PROCEDURE: IS_GL_PERIOD_CLOSED                                                                    |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 25-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    FUNCTION IS_GL_PERIOD_CLOSED(p_gl_date         IN DATE
                                ,p_organization_id IN NUMBER) RETURN BOOLEAN IS

      CURSOR c_inv_cur(pc_appl_id IN NUMBER) IS
        SELECT 'N'
          FROM gl_period_statuses ps
             , hr_operating_units ou
         WHERE ou.organization_id     = p_organization_id
           AND TRUNC(p_gl_date)      <= TRUNC(SYSDATE)
           AND TRUNC(p_gl_date) BETWEEN TRUNC(ps.start_date)
                                    AND TRUNC(ps.end_date)
           AND ps.closing_status     IN ('O', 'F')
           AND ps.set_of_books_id     = ou.set_of_books_id
           AND ps.application_id      = pc_appl_id
           AND NVL(ps.adjustment_period_flag, 'N')
                                      = 'N';
                                        
      l_closed  VARCHAR2(1) DEFAULT 'N';
      l_appl_id NUMBER;
    BEGIN
      l_appl_id := XXML_OEBS_UTILS_PKG.app_from_short_name_to_id('SQLAP');
        
      OPEN c_inv_cur(l_appl_id);
        FETCH c_inv_cur INTO l_closed;
      CLOSE c_inv_cur;

      RETURN (NVL(l_closed, 'N') = 'Y');
    END IS_GL_PERIOD_CLOSED;
  
    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: RECUPERA INFORMACAO DE LOOKUPS                                                          |
    |                                                                                                   |
    | PROCEDURE: GET_LOOKUP_VALUES                                                                      |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 25-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    FUNCTION GET_LOOKUP_VALUES(p_lookup_type IN VARCHAR2
                              ,p_lookup_code IN VARCHAR2 DEFAULT NULL
                              ,p_meaning     IN VARCHAR2 DEFAULT NULL) RETURN fnd_lookup_values_vl%ROWTYPE IS

      x_lkp_type fnd_lookup_values_vl%ROWTYPE;
    BEGIN
      BEGIN
        SELECT *
          INTO x_lkp_type
          FROM fnd_lookup_values_vl
         WHERE lookup_type  = p_lookup_type
           AND (lookup_code = p_lookup_code
            OR  meaning     = p_meaning);
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
      END;
         
      RETURN x_lkp_type;
           
    END GET_LOOKUP_VALUES;

    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: RECUPERA SUFIXO DO VENDOR_SITE_CODE                                                     |
    |                                                                                                   |
    | PROCEDURE: GET_SUPPLIER_SUFFIX                                                                    |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 17-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    FUNCTION GET_SUPPLIER_SUFFIX(p_lookup_type IN VARCHAR2
                                ,p_source      IN VARCHAR2) RETURN VARCHAR2 IS
      l_lkp_type fnd_lookup_values_vl%ROWTYPE;
    BEGIN
      l_lkp_type := GET_LOOKUP_VALUES(p_lookup_type, p_source);
      
      RETURN l_lkp_type.attribute1;

    END GET_SUPPLIER_SUFFIX;

    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: RECUPERA TIPO FORNECEDOR PELO NUMERO DE CARACTERES DO CPF/CNPJ                          |
    |                                                                                                   |
    | PROCEDURE: GET_SUPPLIER_TYPE                                                                      |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 17-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    FUNCTION GET_SUPPLIER_TYPE(p_cpf_cnpj IN VARCHAR2) RETURN VARCHAR2 IS
      l_size NUMBER;
    BEGIN
      l_size := LENGTH(p_cpf_cnpj);
      
      IF (l_size > 11) THEN
        RETURN 'J';
      ELSE
        RETURN 'F';
      END IF;

    END GET_SUPPLIER_TYPE;
    
    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: RECUPERA VALORES PRE-DEFINIDOS PARA DERIVACAO POR ORIGEM E APLICACAO                    |
    |                                                                                                   |
    | PROCEDURE: GET_HEADER_PARAMS                                                                      |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 17-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    FUNCTION GET_HEADER_PARAMS(p_source    IN VARCHAR2
                              ,p_supp_type IN VARCHAR2) RETURN xxml_param_headers%ROWTYPE IS
           
      l_param_gerais xxml_param_headers%ROWTYPE;
    BEGIN
      BEGIN
        SELECT *
          INTO l_param_gerais
          FROM xxml_param_headers
         WHERE source        = p_source
           AND supplier_type = p_supp_type
           AND enabled_flag  = 'Y'
           AND (end_active_date >= TRUNC(SYSDATE)
            OR  end_active_date IS NULL);
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
      END;
      
      RETURN l_param_gerais;
    END GET_HEADER_PARAMS;
    
    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: RECUPERA ITEM POR ORGANIZACAO E TIPO FORNECEDOR / INTEGRACAO                            |
    |                                                                                                   |
    | PROCEDURE: GET_DEFAULT_ITEM                                                                       |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 17-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    FUNCTION GET_LINES_PARAM(p_source    IN VARCHAR2
                            ,p_supp_type IN VARCHAR2) RETURN VARCHAR2 IS
                             
      l_lkp_code VARCHAR2(30);
      l_item     MTL_SYSTEM_ITEMS_B.segment1%TYPE;
    BEGIN    
      BEGIN
        SELECT item_code
          INTO l_item
          FROM xxml_param_lines
         WHERE source        = p_source
           AND supplier_type = p_supp_type
           AND enabled_flag  = 'Y'
           AND (end_active_date >= TRUNC(SYSDATE)
            OR  end_active_date IS NULL);
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
      END;
      
      RETURN l_item;
    END GET_LINES_PARAM;
    
    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: ATUALIZA LINHA EM LOOKUP CUSTOMIZADA *OAF                                               |
    |                                                                                                   |
    | PROCEDURE: UPDATE_LOOKUP_VALUES                                                                   |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 25-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    PROCEDURE UPDATE_LOOKUP_VALUES(p_lookup_type  IN VARCHAR2
                                  ,p_lookup_code  IN VARCHAR2
                                  ,p_meaning      IN VARCHAR2 DEFAULT NULL
                                  ,p_description  IN VARCHAR2 DEFAULT NULL
                                  ,p_tag          IN VARCHAR2 DEFAULT NULL
                                  ,p_enabled_flag IN VARCHAR2
                                  ,x_msg_retorno OUT VARCHAR2) IS
                                  
       l_cust_type             VARCHAR2(4);
       l_enabled_flag          VARCHAR2(250);
       l_security_group_id     NUMBER;
       l_view_application_id   NUMBER;
       l_tag                   VARCHAR2 (250);
       l_meaning               VARCHAR2 (250);
       l_description           VARCHAR2 (250);
       
       l_lkp_vl fnd_lookup_values_vl%ROWTYPE;

    BEGIN
      l_cust_type := SUBSTR(p_lookup_type, 1, 4);
      
      IF (l_cust_type <> 'XXML') THEN
        x_msg_retorno := 'Lookup informada nao apresenta nome padrao de customizacao: XXML';
        RETURN;
      END IF;
      
      l_lkp_vl := GET_LOOKUP_VALUES(p_lookup_type => p_lookup_type, p_lookup_code => p_lookup_code);
                    
      IF (l_lkp_vl.security_group_id IS NULL OR l_lkp_vl.view_application_id IS NULL) THEN
        x_msg_retorno := 'Nao identificado valores para o tipo '||p_lookup_type;
        RETURN;
      END IF;

      l_meaning      := NVL(p_meaning, l_lkp_vl.meaning);
      l_description  := NVL(p_description, l_lkp_vl.description);
      l_tag          := NVL(p_tag, l_lkp_vl.tag);
      l_enabled_flag := NVL(p_enabled_flag, l_lkp_vl.enabled_flag);

      BEGIN
        fnd_lookup_values_pkg.update_row
                    (x_lookup_type           => p_lookup_type,
                     x_security_group_id     => l_lkp_vl.security_group_id,
                     x_view_application_id   => l_lkp_vl.view_application_id,
                     x_lookup_code           => p_lookup_code,
                     x_tag                   => l_tag,
                     x_attribute_category    => NULL,
                     x_attribute1            => NULL,
                     x_attribute2            => NULL,
                     x_attribute3            => NULL,
                     x_attribute4            => NULL,
                     x_enabled_flag          => l_enabled_flag,
                     x_start_date_active     => NULL,
                     x_end_date_active       => NULL,
                     x_territory_code        => NULL,
                     x_attribute5            => NULL,
                     x_attribute6            => NULL,
                     x_attribute7            => NULL,
                     x_attribute8            => NULL,
                     x_attribute9            => NULL,
                     x_attribute10           => NULL,
                     x_attribute11           => NULL,
                     x_attribute12           => NULL,
                     x_attribute13           => NULL,
                     x_attribute14           => NULL,
                     x_attribute15           => NULL,
                     x_meaning               => l_meaning,
                     x_description           => l_description,
                     x_last_update_date      => TRUNC(SYSDATE),
                     x_last_updated_by       => fnd_global.user_id,
                     x_last_update_login     => fnd_global.user_id);

        COMMIT;
      EXCEPTION
         WHEN OTHERS THEN
           x_msg_retorno := p_lookup_code || ' - Excecao interna - ' || SQLERRM;
      END;
    EXCEPTION
       WHEN OTHERS THEN
         x_msg_retorno := 'Falha programa atualizacao '||SQLERRM;
    END UPDATE_LOOKUP_VALUES;
    
    /*
    +===================================================================================================+
    |                                                                                                   |
    | OBJETIVO: INSERE LINHA EM LOOKUP CUSTOMIZADA *OAF                                                 |
    |                                                                                                   |
    | PROCEDURE: PUT_LOOKUP_VALUES                                                                      |
    |                                                                                                   |
    | CREATE DATE                CREATE BY                                                              |
    | --------------------------------------------------------------------------------------------------+
    | 25-07-2017                 RAFAEL LUIZ DE FARIA OLIVEIRA                                          |
    |                            ANALISTA ORACLE/SOA                                                    |
    +===================================================================================================+
    */
    PROCEDURE PUT_LOOKUP_VALUES(p_lookup_type  IN VARCHAR2
                               ,p_lookup_code  IN VARCHAR2
                               ,p_meaning      IN VARCHAR2 DEFAULT NULL
                               ,p_description  IN VARCHAR2 DEFAULT NULL
                               ,p_tag          IN VARCHAR2 DEFAULT NULL
                               ,x_msg_retorno OUT VARCHAR2) IS
                               
      l_rowid                 VARCHAR2(100) := 0;
      l_cust_type             VARCHAR2(4);
      
      l_security_group_id     NUMBER;
      l_view_application_id   NUMBER;
                                  
    BEGIN
      l_cust_type := SUBSTR(p_lookup_type, 1, 4);
      
      IF (l_cust_type <> 'XXML') THEN
        x_msg_retorno := 'Lookup informada nao apresenta nome padrao de customizacao: XXML';
        RETURN;
      END IF;
    
      BEGIN
        SELECT security_group_id,
               view_application_id
          INTO l_security_group_id
              ,l_view_application_id
          FROM fnd_lookup_types_tl
         WHERE lookup_type = p_lookup_type
           AND language    = USERENV('LANG');
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          x_msg_retorno := 'Tipo informado nao encontrado';
          RETURN;
      END;
   
      BEGIN
        FND_LOOKUP_VALUES_PKG.insert_row (
           x_rowid                 => l_rowid,
           x_lookup_type           => p_lookup_type,
           x_security_group_id     => l_security_group_id,
           x_view_application_id   => l_view_application_id,
           x_lookup_code           => p_lookup_code,
           x_tag                   => p_tag,
           x_attribute_category    => NULL,
           x_attribute1            => NULL,
           x_attribute2            => NULL,
           x_attribute3            => NULL,
           x_attribute4            => NULL,
           x_enabled_flag          => 'Y',
           x_start_date_active     => SYSDATE,
           x_end_date_active       => NULL,
           x_territory_code        => NULL,
           x_attribute5            => NULL,
           x_attribute6            => NULL,
           x_attribute7            => NULL,
           x_attribute8            => NULL,
           x_attribute9            => NULL,
           x_attribute10           => NULL,
           x_attribute11           => NULL,
           x_attribute12           => NULL,
           x_attribute13           => NULL,
           x_attribute14           => NULL,
           x_attribute15           => NULL,
           x_meaning               => p_meaning,
           x_description           => p_description,
           x_creation_date         => SYSDATE,
           x_created_by            => FND_GLOBAL.user_id,
           x_last_update_date      => SYSDATE,
           x_last_updated_by       => FND_GLOBAL.user_id,
           x_last_update_login     => FND_GLOBAL.login_id);
           
        COMMIT;
      EXCEPTION
        WHEN OTHERS THEN
         x_msg_retorno := 'Falha na API: '||SQLERRM;
      END;
    EXCEPTION
       WHEN OTHERS THEN
         x_msg_retorno := 'Falha programa de insercao: '||SQLERRM;
    END PUT_LOOKUP_VALUES;

END XXML_FIN_INT_PARAMETER_PKG;
/
EXIT


