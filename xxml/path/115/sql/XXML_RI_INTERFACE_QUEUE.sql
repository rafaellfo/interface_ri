begin
  sys.dbms_aqadm.create_queue_table(
    queue_table => 'XXML.XXML_RI_INTEGRACAO_QT',
    queue_payload_type => 'RAW',
    sort_list => 'ENQ_TIME',
    compatible => '10.0.0',
    primary_instance => 0,
    secondary_instance => 0);
    end;
/

--

begin
  sys.dbms_aqadm.create_queue(
    queue_name => 'XXML.XXML_RI_INTEGRACAO_Q',
    queue_table => 'XXML.XXML_RI_INTEGRACAO_QT',
    queue_type => sys.dbms_aqadm.normal_queue,
    max_retries => 5,
    retry_delay => 0,
    retention_time => 0);
end;
/

-- Start para enqueue e dequeue

BEGIN
 DBMS_AQADM.start_queue('XXML.XXML_RI_INTEGRACAO_Q', TRUE, TRUE );
END;
/

--

SELECT * FROM XXML.XXML_RI_INTEGRACAO_QT
