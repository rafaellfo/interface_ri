CREATE TABLE XXML.XXML_PARAM_HEADERS
(source                 VARCHAR2(30) NOT NULL
,supplier_type          VARCHAR2(30) NOT NULL
,terms_name             VARCHAR2(50)
,fiscal_document_model  VARCHAR2(30)
,invoice_type_code      VARCHAR2(15)
,income_code            VARCHAR2(30) -- pagamento gps
,series                 VARCHAR2(30)
,icms_type              VARCHAR2(30)
,attribute_category     VARCHAR2(30)
,enabled_flag           VARCHAR2(1) DEFAULT 'Y'
,start_active_date      DATE
,end_active_date        DATE
,creation_date          DATE DEFAULT SYSDATE
,created_by             NUMBER
,last_update_date       DATE DEFAULT SYSDATE
,last_updated_by        NUMBER
,last_update_login      NUMBER)
/

CREATE UNIQUE INDEX XXML.XXML_PARAM_HEADERS_U1 ON XXML.XXML_PARAM_HEADERS(integration_type, supplier_type, fiscal_document_model, invoice_type_code)
/

CREATE SYNONYM XXML_PARAM_HEADERS FOR XXML.XXML_PARAM_HEADERS
/

CREATE SYNONYM XXML_PARAM_HEADERS_S FOR XXML.XXML_PARAM_HEADERS_S
/
EXIT
-- DROP TABLE XXML.XXML_PARAM_HEADERS
