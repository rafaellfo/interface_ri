--------------------------------------------------------------------------------
-- HEADERS

INSERT INTO xxml_param_headers VALUES ('CONTA_ENERGIA', 'J', NULL, 'NFCEE', 'E066', NULL, NULL, 'NORMAL', 'Conta de Energia El�trica', 'Y', SYSDATE, NULL, SYSDATE, -1, SYSDATE, -1, -1)
/

INSERT INTO xxml_param_headers VALUES ('MLVOCE', 'F', 'A VISTA', 'RPA', 'E073', NULL, 'B', 'NOT APPLIED', NULL, 'Y', SYSDATE, NULL, SYSDATE, -1, SYSDATE, -1, -1)
/

INSERT INTO xxml_param_headers VALUES ('FREELANCER', 'F', 'A VISTA', 'RPA', 'E006', NULL, 'B', 'NOT APPLIED', NULL, 'Y', SYSDATE, NULL, SYSDATE, -1, SYSDATE, -1, -1)
/

--------------------------------------------------------------------------------
-- LINES

INSERT INTO xxml_param_lines VALUES ('CONTA_ENERGIA', 'J', '2621999', 'SERVICO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y',  SYSDATE, NULL, SYSDATE, -1, SYSDATE, -1, -1)
/

INSERT INTO xxml_param_lines VALUES ('MLVOCE', 'F', '3125854', 'SERVICO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y',  SYSDATE, NULL, SYSDATE, -1, SYSDATE, -1, -1)
/

INSERT INTO xxml_param_lines VALUES ('FREELANCER', 'F', '2297200', 'SERVICO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N',  SYSDATE, SYSDATE, SYSDATE, -1, SYSDATE, -1, -1)
/
