CREATE TABLE XXML.XXML_PARAM_LINES
(source             VARCHAR2(30) NOT NULL
,supplier_type      VARCHAR2(30) NOT NULL
,item_code          VARCHAR2(40)
,item_type          VARCHAR2(30)
,category_attribute VARCHAR2(30)
,attribute1         VARCHAR2(150)
,attribute2         VARCHAR2(150)
,attribute3         VARCHAR2(150)
,attribute4         VARCHAR2(150)
,attribute5         VARCHAR2(150)
,attribute6         VARCHAR2(150)
,attribute7         VARCHAR2(150)
,attribute8         VARCHAR2(150)
,attribute9         VARCHAR2(150)
,attribute10        VARCHAR2(150)
,attribute11        VARCHAR2(150)
,attribute12        VARCHAR2(150)
,attribute13        VARCHAR2(150)
,attribute14        VARCHAR2(150)
,attribute15        VARCHAR2(150)
,enabled_flag       VARCHAR2(1) DEFAULT 'Y'
,start_active_date  DATE
,end_active_date    DATE
,creation_date      DATE DEFAULT SYSDATE
,created_by         NUMBER
,last_update_date   DATE DEFAULT SYSDATE
,last_updated_by    NUMBER
,last_update_login  NUMBER)
/

CREATE SYNONYM XXML_PARAM_LINES FOR XXML.XXML_PARAM_LINES
/
EXIT




--          DROP TABLE XXML.XXML_PARAM_LINES
