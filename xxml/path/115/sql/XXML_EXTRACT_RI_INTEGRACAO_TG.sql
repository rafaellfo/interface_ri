CREATE OR REPLACE TRIGGER XXML.XXML_EXTRACT_RI_INTEGRACAO_TG
 AFTER
  INSERT
 ON cll.cll_f395_custom_extract_data
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
    WHEN (NEW.INTEGRATION_CODE in ('XXML_RI_EO_001', 'XXML_RI_EO_002', 'XXML_RI_IFACE_001'))
DECLARE
-- +=================================================================+
-- | FILENAME                                                        |
-- |   XXML_EXTRACT_RI_EO_001_TG.tg                                  |
-- |                                                                 |
-- | PURPOSE                                                         |
-- |  Dinamic Extract Data                                           |
-- |                                                                 |
-- | DESCRIPTION                                                     |
-- |   Processa Retorno Concurrent                                   |
-- |                                                                 |
-- | CREATED BY  Rafael Luiz de Faria Oliveira  09/08/2017           |
-- |                                                                 |
-- | UPDATED BY  <Desenvolvedor>      -     <data>                   |
-- +=================================================================+

    enqueue_options             dbms_aq.enqueue_options_t;
    message_properties          dbms_aq.message_properties_t;
    message_handle              raw(16);
    msg_in                      raw(2000);
    msg_varchar                 varchar2(4000);
    --
    v_Doc                       XmlDom.DomDocument;
    v_RootNode                  XmlDom.DomNode;
    v_ValueIdRetorno            VARCHAR2(4000);
    v_ValueCodigoRetorno        VARCHAR2(4000);
    v_ValueOrganizacao          VARCHAR2(4000);
    v_ValueLastUpdateDate       VARCHAR2(4000);
    v_ValueCodigoParceiro       VARCHAR2(4000);
    v_ValueProtocolo            VARCHAR2(4000);
    v_Status                    CHAR(1);
    v_Descricao                 VARCHAR2(4000);

BEGIN

    v_Doc := XmlDom.NewDomDocument(:new.xmldata);
    v_RootNode := XmlDom.MakeNode(XmlDom.GetDocumentElement(v_Doc));

    IF :NEW.INTEGRATION_CODE IN ('XXML_RI_EO_001', 'XXML_RI_EO_002') THEN
       v_Status := 'S';
       --
       v_ValueCodigoParceiro := XmlDom.GetNodeValue(
       XslProcessor.SelectSingleNode(v_RootNode, '/CllF395/CllF189EntryOperations/AttributeCategory/text()'));
       --
       v_ValueProtocolo := XmlDom.GetNodeValue(
       XslProcessor.SelectSingleNode(v_RootNode, '/CllF395/CllF189EntryOperations/Attribute2/text()'));
       --
       v_ValueIdRetorno := XmlDom.GetNodeValue(
       XslProcessor.SelectSingleNode(v_RootNode, '/CllF395/CllF189EntryOperations/OperationId/text()'));
       --
       v_ValueCodigoRetorno := XmlDom.GetNodeValue(
       XslProcessor.SelectSingleNode(v_RootNode, '/CllF395/CllF189EntryOperations/Source/text()'));
       --
       v_ValueOrganizacao := XmlDom.GetNodeValue(
       XslProcessor.SelectSingleNode(v_RootNode, '/CllF395/CllF189EntryOperations/OrganizationId/text()'));
       --
       v_ValueLastUpdateDate := XmlDom.GetNodeValue(
       XslProcessor.SelectSingleNode(v_RootNode, '/CllF395/CllF189EntryOperations/CreationDate/text()'));
       --
       v_Descricao := 'Sucesso';

     ELSE
       v_Status := 'E';
       --
       v_ValueCodigoParceiro := XmlDom.GetNodeValue(
       XslProcessor.SelectSingleNode(v_RootNode, '/CllF395/CllF189InvoicesInterface/Source/text()'));
       --
       v_ValueProtocolo := XmlDom.GetNodeValue(
       XslProcessor.SelectSingleNode(v_RootNode, '/CllF395/CllF189InvoicesInterface/CeoAttribute2/text()'));
       --
       v_ValueIdRetorno := XmlDom.GetNodeValue(
       XslProcessor.SelectSingleNode(v_RootNode, '/CllF395/CllF189InvoicesInterface/InterfaceInvoiceId/text()'));
       --
       v_ValueCodigoRetorno := -1;
       --
       v_ValueOrganizacao := XmlDom.GetNodeValue(
       XslProcessor.SelectSingleNode(v_RootNode, '/CllF395/CllF189InvoicesInterface/OrganizationId/text()'));
       --
       v_ValueLastUpdateDate := XmlDom.GetNodeValue(
       XslProcessor.SelectSingleNode(v_RootNode, '/CllF395/CllF189InvoicesInterface/CreationDate/text()'));
       --
       v_Descricao := 'ERRO AO EXECUTAR PROGRAMA DE IMPORTACAO DO RI.';

       BEGIN
         FOR X IN (SELECT ERROR_MESSAGE AS MSG_ERRO
                     FROM cll_f189_interface_errors
                    WHERE interface_invoice_id = v_ValueIdRetorno) LOOP
                    
           V_DESCRICAO := V_DESCRICAO ||chr(09)||X.MSG_ERRO;
         END LOOP;
       EXCEPTION
         WHEN OTHERS THEN
           V_DESCRICAO := 'ERRO NA EXECUCAO DO CONCURRENTE DE IMPORTACAO DA NF.';
       END;
       
       v_descricao := XXML_OEBS_UTILS_PKG.translate_f(V_DESCRICAO);

     END IF;

     msg_varchar :=
'<?xml version="1.0" encoding="UTF-8"?>
<InserirRequest xmlns="http://service.magazineluiza.com.br/corporativo/ws/v1/notificarParceiro">
   <CodigoParceiro>'||v_ValueCodigoParceiro||'</CodigoParceiro>
   <CodigoAplicacao>'||'CRIAR_RECEBIMENTO'||'</CodigoAplicacao>
   <IdRastreamento>'||v_ValueProtocolo||'</IdRastreamento>
   <DadosAdicionais>
            <IdRetorno>'||v_ValueIdRetorno||'</IdRetorno>
            <CodigoRetorno>'||v_ValueCodigoRetorno||'</CodigoRetorno>
            <Organizacao>'||v_ValueOrganizacao||'</Organizacao>
            <Status>'||v_Status||'</Status>
            <Descricao>'||v_Descricao||'</Descricao>
            <DataExecucao>'||v_ValueLastUpdateDate||'</DataExecucao>
    </DadosAdicionais>
</InserirRequest>';

        msg_in := utl_raw.cast_to_raw(msg_varchar);

        dbms_aq.enqueue(queue_name           => 'XXML.XXML_RI_INTEGRACAO_Q',
                        enqueue_options      => enqueue_options,
                        message_properties   => message_properties,
                        payload              => msg_in,
                        msgid                => message_handle);

EXCEPTION
  WHEN OTHERS THEN
  RAISE_APPLICATION_ERROR(-20506, 'Erro Trigger XXML_EXTRACT_RI_INTEGRACAO_TG.');
END;
/
EXIT
